import os
import shutil


def init_same_epoch(elem_acos,number,plsys,initradau):

    # Takes the objects and planets to the same epoch

    # Create directories

    if os.path.exists('start'):
        shutil.rmtree('start')
    if os.path.exists('back'):
        shutil.rmtree('back')

    os.mkdir('start')
    os.mkdir('back')
    os.mkdir('back/evol/')
    os.mkdir('back/osc/')
    os.mkdir('back/ps/')
    os.mkdir('back/gif/')

    # Create symbolic links

    os.system('ln -s ' + elem_acos + ' elements')
    os.system('ln -s ' + number)
    os.system('ln -s ' + plsys)

    # Remove files if exists

    files = ['startval','sysoutinit','restart','osculate','sysoutinteg']
    for i in range(len(files)):
        if os.path.isfile(files[i]):
            os.remove(files[i])

    # Run initradau_2000

    NPl = "8"
    NKast = "1"
    Ntiss = "5"
    Nobj = "1"
    XL = ".1"
    LL = "10"

    temp = []
    count = 0
    for i in open('number'):
        i = i.replace('\n','')
        os.system(initradau + '<<eof\n'
                  + '    ' + NPl + '       ' + NKast + '\t  ' + Ntiss + '\t\n'
                  + '    ' + Nobj + '\t\t\t\t\n'
                  + i + '  ' + i + '\t\t\n'
                  + '   ' + XL + '\t' + LL + '\t\t\t\neof')

        temp.append("start" + str(i))
        os.system('mv startval start/' + temp[count])

        count = count + 1


def radaugra_evol(dt,ymax,radauint):

    # Runs radau integrator

    start = []
    osc = []
    app = []
    count = 0
    for i in open('number'):
        i = i.replace('\n', '')
        start.append("start" + str(i))
        osc.append("osc" + str(i))
        app.append("app" + str(i))

        shutil.copyfile('start/' + start[count],'startval')

        # Set parameters

        PrintPlSys = "1"
        Epoch = 2451545
        Epoch0 = 2000
        year0 = 365.25

        OutInt = dt
        FinalJD = Epoch + (ymax - Epoch0) * year0

        # Remove files if exists

        files2 = ['.t', 'osculate.old', 'approach.old', 'sysoutinteg.old', 'restart.old']
        for j in range(len(files2)):
            if os.path.isfile(files2[j]):
                os.remove(files2[j])

        if count == 0:
            print("Integrating to JD " + str(FinalJD) + ". Output every " + str(OutInt) + " days.")

        # Run integration

        XL = ".1"
        LL =  "12"

        print("Integrating object " + str(i))
        os.system(radauint + '<<eof\n'
                  + '    ' + str(FinalJD) + '\t' + str(OutInt) + ' \t    \n'
                  + '    ' + XL + '\t ' + LL + '\t\t\t    \n'
                  + '    ' + PrintPlSys + ' \t\t    \n'
                  + 'eof')

        os.system('mv osculate ' + osc[count])
        os.system('mv approach ' + app[count])

        count = count + 1

        os.system('mv sysoutinteg sysoutinteg.old')
        os.system('mv restart restart.old')
        os.system('mv app* back/evol/')
        os.system('mv osc* back/osc/')


def year_back_T_moid(year_t_moid):

    # Calculates orbital elements, T and MOID (with respect to every planet), for each object
    # in every step of integration

    os.chdir('back/osc/')

    files3 = ['number', 'plsys']
    for j in range(len(files3)):
        if os.path.isfile(files3[j]):
            os.remove(files3[j])

    os.system('ln -s ../../number .')
    os.system('ln -s ../../plsys .')

    osc = []
    evol = []
    count = 0
    for i in open('number'):
        i = i.replace('\n', '')

        files4 = ['output020','output021','output022','output023','output024','output025','output026','osculate']

        for j in range(len(files4)):
            if os.path.exists(files4[j]):
                os.remove(files4[j])

        osc.append("osc" + str(i))
        evol.append("evol" + str(i))
        os.system('ln -s ' + osc[count] + ' osculate')

        print("Calculating elements for object " + str(i))
        os.system(year_t_moid + '>aux')

        os.system('mv output027 ' + evol[count])
        os.system('mv ' + evol[count] + ' ../evol/')

        count = count + 1

    os.remove('aux')
