import os
import shutil
import wget
import gzip


def download_elements_asteroids():

    # Download orbital elements from MPC catalog

    wget.download('https://www.minorplanetcenter.net/iau/MPCORB/MPCORB.DAT.gz')
    with gzip.open('MPCORB.DAT.gz', 'rb') as f_in:
        with open('MPCORB.dat', 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
    os.remove('MPCORB.DAT.gz')


def download_elements_comets():

    # Download orbital elements from JPL catalog

    if os.path.isfile("ELEMENTS.COMET"):
        os.remove("ELEMENTS.COMET")

    wget.download('https://ssd.jpl.nasa.gov/dat/ELEMENTS.COMET')
