import os
import re


def create_names():

    files = ['number','names','names_mpc','number_names','names_jf','names_cen','names_aho',
             'acos_jf','acos_cen','acos_aho']
    for i in range(len(files)):
        if os.path.exists(files[i]):
            os.remove(files[i])

    with open('acos.txt') as f1:
        with open(files[0], 'a') as f2:
            for ln in f1:
                sp = ln.split(' ')
                f2.write(sp[0] + '\n')

    with open('elem_acos') as f3:
        with open(files[1],'a') as f4:
            with open(files[2],'a') as f5:
                with open(files[3],'a') as f6:

                    for ln2 in f3:
                        ln2 = re.sub(' +', ' ', ln2)
                        sp2 = ln2.split(' ')

                        if sp2[0] == '':
                            f4.write(sp2[11] + '\n')
                            f5.write(sp2[12] + '\n')
                            p0 = "%6.6s" % str(sp2[1])
                            p1 = "%9.9s" % str(sp2[11])
                            p2 = "%7.7s" % str(sp2[12])

                            with open('acos.txt') as f7:
                                for ln3 in f7:
                                    ln3 = re.sub(' +', ' ', ln3)
                                    sp3 = ln3.split(' ')

                                    if sp2[1] == sp3[0]:
                                        p3 = sp3[1]

                            f6.write(p0 + '  ' + p1 + '  ' + p2 + '  ' + p3)
                        else:
                            f4.write(sp2[10] + '\n')
                            f5.write(sp2[11] + '\n')
                            p0 = "%6.6s" % str(sp2[0])
                            p1 = "%9.9s" % str(sp2[10])
                            p2 = "%7.7s" % str(sp2[11])

                            with open('acos.txt') as f8:
                                for ln4 in f8:
                                    ln4 = re.sub(' +', ' ', ln4)
                                    sp4 = ln4.split(' ')
                                    if sp2[0] == sp4[0]:
                                        p3 = sp4[1]

                            f6.write(p0 + '  ' + p1 + '  ' + p2 + '  ' + p3)

    with open(files[3]) as f9:
        with open(files[4],'a') as f10:
            with open(files[5],'a') as f11:
                with open(files[6],'a') as f12:

                    for ln5 in f9:
                        ln5 = re.sub(' +', ' ', ln5)
                        sp5 = ln5.split(' ')

                        if sp5[0] == '':
                            if sp5[4] == '1\n':
                                f10.write(sp5[2] + '\n')
                            elif sp5[4] == '2\n':
                                f11.write(sp5[2] + '\n')
                            elif sp5[4] == '3\n':
                                f12.write(sp5[2] + '\n')
                        else:
                            if sp5[3] == '1\n':
                                f10.write(sp5[1] + '\n')
                            elif sp5[3] == '2\n':
                                f11.write(sp5[1] + '\n')
                            elif sp5[3] == '3\n':
                                f12.write(sp5[1] + '\n')


    with open(files[3]) as f9:
        with open(files[7],'a') as f10:
            with open(files[8],'a') as f11:
                with open(files[9],'a') as f12:

                    for ln5 in f9:
                        ln5 = re.sub(' +', ' ', ln5)
                        sp5 = ln5.split(' ')

                        if sp5[0] == '':
                            if sp5[4] == '1\n':
                                f10.write(sp5[1] + '\n')
                            elif sp5[4] == '2\n':
                                f11.write(sp5[1] + '\n')
                            elif sp5[4] == '3\n':
                                f12.write(sp5[1] + '\n')
                        else:
                            if sp5[3] == '1\n':
                                f10.write(sp5[0] + '\n')
                            elif sp5[3] == '2\n':
                                f11.write(sp5[0] + '\n')
                            elif sp5[3] == '3\n':
                                f12.write(sp5[0] + '\n')