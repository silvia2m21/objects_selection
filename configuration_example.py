
def config():

    # Change your configurations:

    # current_date = month and year in the format MM-YYYY
    # results = where you want to put your results
    # programs = the path of acos repo
    # option = select option 1 if you run this program for the fist time (all the folders will be created)
    #          select 2 if you just want to create the folders for the current date.

    ########################################################################################################

    current_date = 'MM-YYYY'
    results = '/home/username/Documents/results/'
    programs = '/home/username/Documents/acos/'
    option = '2'

    return current_date, results, programs, option


