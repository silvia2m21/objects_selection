import os
import re
import shutil


def find_acos(infile,grep_lines):

    # Remove files if exists

    files = ['lines', 'file', 'elem_acos', 'elem_acos_aeiTMOID', 'number_acos', 'qvsmag.acos']

    for i in range(len(files)):
        if os.path.exists(files[i]):
            os.remove(files[i])

    # Create file lines with acos numbers sorted

    with open('file','a') as f1:
        with open(infile) as f2:
            for ln in f2:
                sp = ln.split(' ')
                f1.write("%6s" % int(sp[0])+'\n')

    list1 = []
    with open('file') as f3:
        for ln2 in f3:
            list1.append(ln2.strip())
    list1.sort(key=int)

    with open('lines','a') as f4:
        for ln3 in list1:
            f4.write(ln3+'\n')

    os.remove('file')

    # Create file elem_acos with the orbital elements of acos
    # Create file elem_acos_aeiTMOID with aeiTMOID of acos

    os.system(grep_lines + '< elem_mpcorb.prec > elem_acos')
    os.system(grep_lines + '< elem_ast_aeiTMOID.prec > elem_acos_aeiTMOID')

    # Move lines to number_acos

    shutil.copyfile('lines','number_acos')

    # Create file qvsmag.acos with q vs mag of acos

    with open('qvsmag.acos','a') as f5:
        with open('elem_acos','r') as f6:
            for ln4 in f6:
                ln4 = re.sub(' +', ' ', ln4)
                sp = ln4.split(' ')
                if sp[0] == '':
                    p0 = "%10.7f" % (float(sp[2])*(float(1)-float(sp[3])))
                    p1 = "%5.2f" % float(sp[9])
                else:
                    p0 = "%10.7f" % (float(sp[1])*(float(1)-float(sp[2])))
                    p1 = "%5.2f" % float(sp[8])
                f5.write(p0 + ' ' + p1 +'\n')

    os.remove('lines')

