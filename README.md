### **objects selection**

#### Setup 
Run the following commands in prompt:

`$ sudo apt-get install python3-pip gfortran` \

Create virtual environment (optional)

`$ python3 -m venv venv`

Activate virtual environment (optional)

`$ source venv/bin/activate`

Install dependencies

`$ pip3 install python3-wget numpy scipy jdcal requests astroquery `


Copy file `configuration_example.py` into `configuration.py` and change it with your configuration.
 
 
Change the path of your home directory, the month and year, the name of the folder you want to put your results and the
name of the folder were the scripts are.
Select option 1 if you run this program for the fist time (all the folders will be created). Select 2 if you just want
to create the folders for the current date.


Run the main file:

`$ python3 main.py`
