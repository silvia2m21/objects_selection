import os


def files():

    # Files in other
    os.system('gfortran -o ext_jpl_com ext_jpl_com.f')
    os.system('gfortran -o grep_lines grep_lines.f')


def moid():

    # Files in fortran_files/moid
    os.system('make compute_T_moid_sev_planets')


def radau():

    # Files in fortran_files/radau
    os.system('make initradau_2000')
    os.system('make radauint')
    os.system('make year_t_moid')

