import os
import shutil

from create_dirs import create_dirs
from fortran_compiler import files, moid, radau
from download_elements import download_elements_asteroids, download_elements_comets
from create_elements import convert_MPC,TMOID
from include_missing_comets import include_missing_comets, create_files
from criterion import criterion
from find_acos import find_acos
from create_names import create_names
from integration import init_same_epoch, radaugra_evol, year_back_T_moid
from encounters import encounters, no_enc_jf, remove_jf_no_enc
from find_acos_final import find_names, elem_acos_final
from web_table import web_table


# Create directories

fortran_moid, fortran_radau, fortran_files, other_files, ast_elem, criter, ast_acos, ast_integra, com_elem, \
    current_date = create_dirs()


# Compile fortran files

os.chdir(fortran_files)
files()

os.chdir(fortran_moid)
moid()

os.chdir(fortran_radau)
radau()

print(" ")
print("Fortran files compiled.")
print(" ")


# Download orbital elements

print("Downloading orbital elements from MPC catalog...")
print(" ")

os.chdir(ast_elem)
download_elements_asteroids()

print("Orbital elements downloaded.")
print(" ")

print("Downloading orbital elements from JPL catalog...")
print(" ")

os.chdir(com_elem)
download_elements_comets()
os.system(os.path.join(fortran_files,'ext_jpl_com'))

print("Orbital elements downloaded.")
print(" ")


# Create precise elements accordign to MPC parameter of uncertainty

print("Creating elements...")
os.chdir(ast_elem)
convert_MPC(2)
TMOID(os.path.join(fortran_moid,'compute_T_moid_sev_planets'),os.path.join(other_files,'elem_planet'),ast_elem,1)

os.chdir(com_elem)
include_missing_comets(os.path.join(other_files,'missing.txt'),os.path.join(ast_elem,'elem_mpcorb'))
create_files()
TMOID(os.path.join(fortran_moid,'compute_T_moid_sev_planets'),os.path.join(other_files,'elem_planet'),com_elem,2)

print("Files created with orbital elements, T and MOID, for asteroids and comets.")
print(" ")


# Copy files to dir criterion

os.chdir(criter)

shutil.copyfile(os.path.join(com_elem, 'elem_com_aeiTMOID'),'elem_com_aeiTMOID')
shutil.copyfile(os.path.join(ast_elem, 'elem_ast_aeiTMOID.prec'),'elem_ast_aeiTMOID.prec')
shutil.copyfile(os.path.join(other_files, 'elem_planet_orig'), 'elem_planet_orig')


# Apply criterion

print("Aplying criterion...")

os.chdir(criter)
criterion()

print("Files acos.txt and comet.txt created.")
print(" ")


# Remove duplicated files

os.remove('elem_com_aeiTMOID')
os.remove('elem_ast_aeiTMOID.prec')
os.remove('elem_planet_orig')


# Find acos

os.chdir(ast_elem)
find_acos(os.path.join(criter,'acos.txt'),os.path.join(fortran_files,'grep_lines'))

print("Files with orbital elements, T and MOID created for ACOs.")
print(" ")


# Copy files to acos

os.chdir(ast_acos)

shutil.copyfile(os.path.join(ast_elem, 'elem_acos'), 'elem_acos')
shutil.copyfile(os.path.join(criter, 'acos.txt'), 'acos.txt')


# Create names

create_names()

print("Files with names created for different type of ACOs")
print(" ")


# Make integrations

print("Starting integrations...")
print(" ")

os.chdir(ast_integra)

init_same_epoch(os.path.join(ast_acos,'elem_acos'),os.path.join(ast_acos,'number'),
                os.path.join(other_files,'plsys'),os.path.join(fortran_radau,'initradau_2000'))

radaugra_evol(10000.,-8000,os.path.join(fortran_radau,'radauint'))

year_back_T_moid(os.path.join(fortran_radau,'year_t_moid'))

print("Integration finished.")
print(" ")


# Evaluate encounters

os.chdir(os.path.join(ast_integra,'back'))
encounters(current_date)

os.chdir(os.path.join(ast_integra,'back'))
no_enc_jf(os.path.join(other_files,'hill'))

os.chdir(ast_acos)
remove_jf_no_enc(current_date)

print("JF ACOs with no encounters removed.")
print(" ")


# Find acos

os.chdir(ast_acos)
find_names(current_date)

os.chdir(ast_elem)
elem_acos_final(ast_acos,current_date)

print("Files created for final ACOs.")


# Create table for web

os.chdir(ast_elem)
web_table()
print("Table for ACOs web created")