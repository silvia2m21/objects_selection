import os
import re
import shutil
import numpy as np


def encounters(current_date):

    # Gets data about encounters with planets from files obtained during the integration

    planets = ['Venus','Earth-Mo','Mars','Jupiter','Saturn','Uranus','Neptune']
    orbits = ['jf', 'cen', 'aho']
    auxil = ['auxil1','auxil2','auxil3','auxil4','auxil5','auxil6','auxil7']

    for i in range(len(auxil)):
        if os.path.exists(auxil[i]):
            os.remove(auxil[i])

    for j in orbits:

        if os.path.exists('number'):
            os.remove('number')

        for p in range(len(auxil)):
            if os.path.exists(auxil[p]):
                os.remove(auxil[p])

        os.system('ln -s ../../../' + current_date +'/acos_' + j + ' number')

        with open(auxil[0],'a') as f1:
            with open(auxil[1],'a') as f2:
                with open(auxil[2],'a') as f3:
                    with open(auxil[3],'a') as f4:
                        with open(auxil[4],'a') as f5:

                            for i in open('number'):
                                i = i.replace('\n', '')

                                with open('evol/app' + i) as f:
                                    count = 0
                                    wc = 0
                                    aux2 = ''
                                    aux3 = ''
                                    delta_m = 999
                                    delta_m_t = 999*np.ones(len(planets))
                                    wc_pla = np.zeros(len(planets))
                                    wc_del = np.zeros(len(planets))
                                    for ln in f:
                                        sb = re.sub(' +', ' ', ln)
                                        sp = sb.split(' ')

                                        if count > 6:
                                            if sp[2] == 'Jupiter':
                                                wc = wc + 1

                                        if count == 7:
                                            aux2 = ln

                                        if count > 6:
                                            if sp[2] == 'Jupiter':
                                                if float(sp[4]) < float(delta_m):
                                                    delta_m = sp[4]
                                                    aux3 = ln

                                        for k in range(len(planets)):
                                            if count > 6:
                                                if sp[2] == planets[k]:
                                                    wc_pla[k] = wc_pla[k] + 1

                                        for h in range(len(planets)):
                                            if count > 6:
                                                if sp[2] == planets[h]:
                                                    if float(sp[4]) < float(delta_m_t[h]):
                                                        delta_m_t[h] = sp[4]

                                        for l in range(len(wc_del)):
                                            if wc_pla[l] == 0:
                                                wc_del[l] = 99.99999
                                            else:
                                                wc_del[l] = delta_m_t[l]

                                        count = count +1

                                wcounts = str(int(wc_pla[0])) + ' ' + str(int(wc_pla[1])) + ' ' + str(int(wc_pla[2])) +\
                                    ' ' + str(int(wc_pla[3])) + ' ' + str(int(wc_pla[4])) + ' ' + str(int(wc_pla[5])) +\
                                    ' ' + str(int(wc_pla[6]))

                                wcounts2 = str(wc_del[0]) + ' ' + str(wc_del[1]) + ' ' + str(wc_del[2]) +\
                                    ' ' + str(wc_del[3]) + ' ' + str(wc_del[4]) + ' ' + str(wc_del[5]) +\
                                    ' ' + str(wc_del[6])

                                f1.write(i + ' ' + str(wc) + '\n')      # Number of encounters with Jupiter
                                f2.write(aux2)                          # First encounter with any planet
                                f3.write(aux3)                          # Closer encounters with Jupiter
                                f4.write(i + ' ' + wcounts +'\n')       # Number of encounters with the plantes
                                f5.write(i + ' ' + wcounts2 + '\n')     # Planet with closer encounter and distance

        with open(auxil[5],'a') as f:
            f.write('Number ' 'Venus ' 'Earth ' 'Mars ' 'Jupit ' 'Satur ' 'Uranu ' 'Neptu ' + '\n')
            with open(auxil[3]) as ff:
                for ln in ff:
                    sb = re.sub(' +', ' ', ln)
                    sp = sb.split(' ')
                    p1 = "%6d   " % float(sp[0])
                    p2 = "%3d   " % float(sp[1])
                    p3 = "%3d  " % float(sp[2])
                    p4 = "%3d   " % float(sp[3])
                    p5 = "%3d   " % float(sp[4])
                    p6 = "%3d   " % float(sp[5])
                    p7 = "%3d   " % float(sp[6])
                    p8 = "%3d   " % float(sp[7])
                    f.write(p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + '\n')

        with open(auxil[6],'a') as f:
            f.write('Number      ' 'Venus      ' 'Earth       ' 'Mars      ' 'Jupit      ' 'Satur      ' 'Uranu      '
                    '' 'Neptu    ' + '\n')
            with open(auxil[4]) as ff:
                for ln in ff:
                    sb = re.sub(' +', ' ', ln)
                    sp = sb.split(' ')
                    p1 = "%6d   " % float(sp[0])
                    p2 = "%8.5f   " % float(sp[1])
                    p3 = "%8.5f   " % float(sp[2])
                    p4 = "%8.5f   " % float(sp[3])
                    p5 = "%8.5f   " % float(sp[4])
                    p6 = "%8.5f   " % float(sp[5])
                    p7 = "%8.5f   " % float(sp[6])
                    p8 = "%8.5f" % float(sp[7])
                    f.write(p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + '\n')


        os.system('mv auxil1 number_app_Jup_' + j)
        os.system('sort auxil2 -r -n -k 3  > first_enc_' + j)
        os.system('sort auxil3 -n -k 4 > closest_enc_' + j)
        os.system('mv auxil6 number_app_' + j)
        os.system('mv auxil7 closest_app_' + j)

    for p in range(len(auxil)):
        if os.path.exists(auxil[p]):
            os.remove(auxil[p])

    os.chdir('evol')
    if os.path.exists('elements'):
        os.remove('elements')
    if os.path.exists('number'):
        os.remove('number')

    os.system('ln -s ../../../../' + current_date + '/elem_acos elements')
    os.system('ln -s ../../../../' + current_date + '/number number')

    print("Encounters calculated")


def no_enc_jf(hill):

    # Searches for objects (among the jf) that had no encounters with any of the giant planets and
    # their minimum distance is higher than 2.5 x RHill for each giant planet.

    if os.path.exists('auxil'):
        os.remove('auxil')

    nHill = 2.5

    Hill = []
    with open(hill) as f:
        for ln in f:
            sb = re.sub(' +', ' ', ln)
            sp = sb.split(' ')

            Hill.append(nHill*float(sp[2])*np.exp(0.3333*(np.log(float(sp[1])/3/(1+float(sp[1]))))))

    with open('auxil','a') as f:

        with open('number_app_jf') as ff:
            count = 0
            for ln in ff:
                sb = re.sub(' +', ' ', ln)
                sp = sb.split(' ')
                if count > 0:
                    if sp[0] == '':
                        if (float(sp[5]) + float(sp[6]) + float(sp[7]) + float(sp[8])) == 0:
                            f.write(sp[1] + '\n')
                    else:
                        if (float(sp[4]) + float(sp[5]) + float(sp[6]) + float(sp[7])) == 0:
                            f.write(sp[0] + '\n')
                count = count + 1

        with open('closest_app_jf') as ff:
            count = 0
            for ln in ff:
                sb = re.sub(' +', ' ', ln)
                sp = sb.split(' ')
                if count > 0:
                    if sp[0] == '':
                        if ((float(sp[5]) > Hill[3]) & (float(sp[6]) > Hill[4]) & (float(sp[7]) > Hill[5]) & (float(sp[8])
                                                                                                              > Hill[6])):
                            f.write(sp[1] + '\n')
                    else:
                        if ((float(sp[4]) > Hill[3]) & (float(sp[5]) > Hill[4]) & (float(sp[6]) > Hill[5]) & (float(sp[7])
                                                                                                              > Hill[6])):
                            f.write(sp[0] + '\n')
                count = count + 1

    os.system('sort -nu auxil > no_enc_jf')
    os.remove('auxil')

    print("File no_enc_jf created")


def remove_jf_no_enc(current_date):

    # Removes the acos with no encounter from the acos list

    if os.path.exists('acos_jf_remove'):
        os.remove('acos_jf_remove')
    if os.path.exists('acos_jf_sel'):
        os.remove('acos_jf_sel')

    os.system('ln -s ../integra/' + current_date + '/back/no_enc_jf acos_jf_remove')

    with open('acos_jf') as f:
        with open('acos_jf_sel','a') as ff:

            for l in f:
                sb = re.sub(' +', ' ', l)
                sp = sb.split(' ')

                with open('acos_jf_remove') as fff:
                    list = []
                    for ll in fff:
                        sbb = re.sub(' +', ' ', ll)
                        spp = sbb.split(' ')
                        list.append(sp[0]==spp[0])

                    if not(any(list)) == True:
                        ff.write(sp[0])

    print("File acos_jf_sel created")
