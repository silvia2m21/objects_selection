import os
import shutil
from configuration import config


def create_dirs():

    # Create all the directories

    current_date, results, programs, option = config()

    if option == '1':

        if os.path.exists(results):
            shutil.rmtree(results)

        os.mkdir(results)

        os.chdir(results)
        os.mkdir('aster')
        os.mkdir('comet')

        os.chdir('aster')
        os.mkdir('acos')
        os.chdir('acos')
        os.mkdir(current_date)
        os.mkdir('integra')
        os.chdir('integra')
        os.mkdir(current_date)

        os.chdir(os.path.join(results,'aster'))
        os.mkdir('elements')
        os.chdir('elements')
        os.mkdir(current_date)

        os.chdir(os.path.join(results, 'aster'))
        os.mkdir('criterion')
        os.chdir('criterion')
        os.mkdir(current_date)

        os.chdir(os.path.join(results,'comet'))
        os.mkdir('elements')
        os.chdir('elements')
        os.mkdir(current_date)

    elif option == '2':

        os.chdir(results)

        os.chdir('aster')
        os.chdir('acos')
        os.mkdir(current_date)
        os.chdir('integra')
        os.mkdir(current_date)

        os.chdir(os.path.join(results,'aster'))
        os.chdir('elements')
        os.mkdir(current_date)

        os.chdir(os.path.join(results, 'aster'))
        os.chdir('criterion')
        os.mkdir(current_date)

        os.chdir(os.path.join(results,'comet'))
        os.chdir('elements')
        os.mkdir(current_date)

    else:

        print("ERROR: Options must be 1 or 2.")


    # Path to diferent folders

    fortran_moid = os.path.join(programs,'fortran_files/moid/')
    fortran_radau = os.path.join(programs,'fortran_files/radau/')
    fortran_files = os.path.join(programs,'fortran_files/other/')
    other_files = os.path.join(programs,'other_files/')
    ast_elem = os.path.join(results,'aster/elements/',current_date)
    criterion = os.path.join(results,'aster/criterion/',current_date)
    ast_acos = os.path.join(results,'aster/acos/',current_date)
    ast_integra = os.path.join(results,'aster/acos/integra/',current_date)
    com_elem = os.path.join(results,'comet/elements/',current_date)

    print(" ")
    print("Dirs created.")
    print(" ")

    return fortran_moid, fortran_radau, fortran_files, other_files, ast_elem, criterion, ast_acos, ast_integra, \
           com_elem, current_date
