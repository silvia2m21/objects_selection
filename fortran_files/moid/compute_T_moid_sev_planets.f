      program compute_T_moid
      implicit none
      integer iname,i,ip
      real*8 pi,fac
      REAL*8 AP1s(9),AN1s(9),AI1s(9),AE1s(9),AA1s(9)
      REAL*8 AP1,AN1,AI1,AE1,AA1
      REAL*8 AP2,AN2,AI2,AE2,AA2
      real*8 node(3),inode(3),angmom(3),nodep(3),inodep(3),angmomp(3),
     #     cosi,auxil(3),prscal,muti
      real*8 Ts(9),rmin,rmins(9)
      COMMON/ORB1/AP1,AN1,AI1,AE1,AA1
      COMMON/ORB2/AP2,AN2,AI2,AE2,AA2
      pi=atan(1.d0)*4.d0
      fac=pi/180.d0
      open(1,file='elem_planet')
      i=1
 10   read (1,*,end=500) AA1s(i),AE1s(i),AI1s(i),AN1s(i),AP1s(i)
      i=i+1
      goto 10
 500  continue
      ip=i-1
 1    continue
      read (*,*,end=900) iname,AA2,AE2,AI2,AN2,AP2
      node(1)=cos(AN2*fac)
      node(2)=sin(AN2*fac)
      node(3)=0
      inode(1)=-sin(AN2*fac)*cos(AI2*fac)
      inode(2)=cos(AN2*fac)*cos(AI2*fac)
      inode(3)=sin(AI2*fac)
      call prvec(node,inode,auxil)
      call normal(auxil,angmom)
      do i=1,ip
         AA1=AA1s(i)
         AE1=AE1s(i)
         AI1=AI1s(i)
         AN1=AN1s(i)
         AP1=AP1s(i)
         call moid(rmin)
         rmins(i)=rmin
         nodep(1)=cos(AN1*fac)
         nodep(2)=sin(AN1*fac)
         nodep(3)=0
         inodep(1)=-sin(AN1*fac)*cos(AI1*fac)
         inodep(2)=cos(AN1*fac)*cos(AI1*fac)
         inodep(3)=sin(AI1*fac)
         call prvec(nodep,inodep,auxil)
         call normal(auxil,angmomp)
         cosi=prscal(angmom,angmomp)
         Ts(i)=AA1/AA2+2.d0*sqrt(AA2/AA1*(1.d0-AE2*AE2))*cosi
         if (i.eq.1) muti=acos(cosi)/fac
      enddo
      write (*,100) iname,AA2,AE2,muti,(Ts(i),rmins(i),i=1,ip)
100   format(i6,1x,f13.6,2(1x,f11.6),18(1x,f10.5))
      goto 1
 900  continue
      end

