      real*8 function func(V)
      implicit none
      real*8 V(2),h1(3,3),h2(3,3),u1(3),u2(3)
      real*8 V1,V2,RV1(3),RV2(3),diff(3)
      real*8 R1,R2,X1,Y1,X2,Y2
      real*8 zero
      real*8 prscal
      integer i
      COMMON/RXY/R1,R2,X1,Y1,X2,Y2
      zero=0.d0
      V1=V(1)
      V2=V(2)
      call R1R2(V1,V2)
      u1(1)=X1
      u1(2)=Y1
      u1(3)=zero
      u2(1)=X2
      u2(2)=Y2
      u2(3)=zero
      call h1h2(h1,h2)
      call prodmv(RV1,h1,u1)
      call prodmv(RV2,h2,u2)
      do i=1,3
         diff(i)=RV1(i)-RV2(i)
      enddo
      func=prscal(diff,diff)/2.d0
c      print *,sqrt(func*2.d0)
      return
      end

      subroutine dfunc(V,DF)
      implicit none
      REAL*8 V(2),DF(2)
      REAL*8 R1,R2,X1,Y1,X2,Y2
      REAL*8 P1,P2,K,L,M,N
      REAL*8 AP1,AN1,AI1,AE1,AA1
      REAL*8 AP2,AN2,AI2,AE2,AA2
      COMMON/RXY/R1,R2,X1,Y1,X2,Y2
      COMMON/DATA/P1,P2,K,L,M,N
      COMMON/ORB1/AP1,AN1,AI1,AE1,AA1
      COMMON/ORB2/AP2,AN2,AI2,AE2,AA2
      DF(1)=R1/P1*(AE1*R1*Y1+Y1*(K*X2+M*Y2)-(AE1*R1+X1)*(L*X2+N*Y2))
      DF(2)=R2/P2*(AE2*R2*Y2+Y2*(K*X1+L*Y1)-(AE2*R2+X2)*(M*X1+N*Y1))
      return
      end

      SUBROUTINE R1R2(V1,V2)
      implicit none
      REAL*8 V1,V2
      REAL*8 R1,R2,X1,Y1,X2,Y2
      REAL*8 AP1,AN1,AI1,AE1,AA1
      REAL*8 AP2,AN2,AI2,AE2,AA2
      REAL*8 P1,P2,K,L,M,N
      COMMON/RXY/R1,R2,X1,Y1,X2,Y2
      COMMON/ORB1/AP1,AN1,AI1,AE1,AA1
      COMMON/ORB2/AP2,AN2,AI2,AE2,AA2
      COMMON/DATA/P1,P2,K,L,M,N
      R1=P1/(1.d0+AE1*COS(V1))
      R2=P2/(1.d0+AE2*COS(V2))
      X1=R1*COS(V1)
      Y1=R1*SIN(V1)
      X2=R2*COS(V2)
      Y2=R2*SIN(V2)
      return
      end

      subroutine h1h2(h1,h2)
      implicit none
      real*8 h1(3,3),h2(3,3),h3(3,3)
      real*8 h11(3,3),h12(3,3),h21(3,3),h22(3,3),h23(3,3)
      REAL*8 CAP1,SAP1,CAI1,SAI1
      REAL*8 CAP2,SAP2,CAI2,SAI2
      REAL*8 CDAN,SDAN
      real*8 one,zero
      COMMON/ANG1/CAP1,SAP1,CAI1,SAI1
      COMMON/ANG2/CAP2,SAP2,CAI2,SAI2
      COMMON/DANG/CDAN,SDAN
      one=1.d0
      zero=0.d0
      h11(1,1)=one
      h11(1,2)=zero
      h11(1,3)=zero
      h11(2,1)=zero
      h11(2,2)=CAI1
      h11(2,3)=SAI1
      h11(3,1)=zero
      h11(3,2)=-SAI1
      h11(3,3)=CAI1

      h12(3,3)=one
      h12(3,2)=zero
      h12(3,1)=zero
      h12(2,3)=zero
      h12(2,2)=CAP1
      h12(2,1)=SAP1
      h12(1,3)=zero
      h12(1,2)=-SAP1
      h12(1,1)=CAP1

      call prodmm(h1,h11,h12)

      h21(3,3)=one
      h21(3,2)=zero
      h21(3,1)=zero
      h21(2,3)=zero
      h21(2,2)=CDAN
      h21(2,1)=SDAN
      h21(1,3)=zero
      h21(1,2)=-SDAN
      h21(1,1)=CDAN

      h22(1,1)=one
      h22(1,2)=zero
      h22(1,3)=zero
      h22(2,1)=zero
      h22(2,2)=CAI2
      h22(2,3)=SAI2
      h22(3,1)=zero
      h22(3,2)=-SAI2
      h22(3,3)=CAI2

      h23(3,3)=one
      h23(3,2)=zero
      h23(3,1)=zero
      h23(2,3)=zero
      h23(2,2)=CAP2
      h23(2,1)=SAP2
      h23(1,3)=zero
      h23(1,2)=-SAP2
      h23(1,1)=CAP2

      call prodmm(h3,h21,h22)
      call prodmm(h2,h3,h23)

      return
      end

      
	SUBROUTINE KLMN
C
C	Compute K,L,M,N from Sitarski and the upper and lower case P's and Q's
C
	IMPLICIT NONE
	REAL*8 AP1,AN1,AI1,AE1,AA1
	REAL*8 AP2,AN2,AI2,AE2,AA2
	REAL*8 PX1,QX1,PY1,QY1,PZ1,QZ1
	REAL*8 PX2,QX2,PY2,QY2,PZ2,QZ2
        REAL*8 CAP1,SAP1,CAI1,SAI1
        REAL*8 CAP2,SAP2,CAI2,SAI2
        REAL*8 CDAN,SDAN
	REAL*8 P1,P2,K,L,M,N
        REAL*8 R1,R2,X1,Y1,X2,Y2
        REAL*8 DEGRAD,Q1,Q2
	REAL*8 AP1R,AN1R,AI1R
	REAL*8 AP2R,AN2R,AI2R

	COMMON/ORB1/AP1,AN1,AI1,AE1,AA1
	COMMON/ORB2/AP2,AN2,AI2,AE2,AA2
	COMMON/VEC1/PX1,QX1,PY1,QY1,PZ1,QZ1
	COMMON/VEC2/PX2,QX2,PY2,QY2,PZ2,QZ2
        COMMON/ANG1/CAP1,SAP1,CAI1,SAI1
        COMMON/ANG2/CAP2,SAP2,CAI2,SAI2
        COMMON/DANG/CDAN,SDAN
	COMMON/DATA/P1,P2,K,L,M,N
        COMMON/RXY/R1,R2,X1,Y1,X2,Y2
	DEGRAD=datan(1.d0)*4.d0/180.D0
	Q1=(1.-AE1)*AA1
	Q2=(1.-AE2)*AA2
	P1=(1.+AE1)*Q1
	P2=(1.+AE2)*Q2
C---	Compute the capital P, Q values and save in VEC1 and VEC2
	AN1R=AN1*DEGRAD
	AP1R=AP1*DEGRAD
	AI1R=AI1*DEGRAD
	SAP1=SIN(AP1R)
	CAP1=COS(AP1R)
	SAI1=SIN(AI1R)
	CAI1=COS(AI1R)
	PX1=+CAP1
	QX1=-SAP1
	PY1=SAP1*CAI1
	QY1=CAP1*CAI1
	PZ1=SAP1*SAI1
	QZ1=CAP1*SAI1
	AN2R=AN2*DEGRAD
	AP2R=AP2*DEGRAD
	AI2R=AI2*DEGRAD
	SAP2=SIN(AP2R)
	CAP2=COS(AP2R)
	SAI2=SIN(AI2R)
	CAI2=COS(AI2R)
C---    And the lower case p,q values as well as K, L, M, N
	SDAN=SIN(AN2R-AN1R)
	CDAN=COS(AN2R-AN1R)
	PX2=CAP2*CDAN-SAP2*CAI2*SDAN
	PY2=CAP2*SDAN+SAP2*CAI2*CDAN
	PZ2=SAP2*SAI2
	QX2=-SAP2*CDAN-CAP2*CAI2*SDAN
	QY2=-SAP2*SDAN+CAP2*CAI2*CDAN
	QZ2=+CAP2*SAI2
	K=PX1*PX2+PY1*PY2+PZ1*PZ2
	L=QX1*PX2+QY1*PY2+QZ1*PZ2
	M=PX1*QX2+PY1*QY2+PZ1*QZ2
	N=QX1*QX2+QY1*QY2+QZ1*QZ2
	RETURN
	END

