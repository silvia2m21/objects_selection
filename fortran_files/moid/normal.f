C
C-----------------------------------------------------------------------------
C
C SUBROUTINE NORMAL COMPUTES THE UNIT VECTOR V FOR A GIVEN VECTOR X     
C                                                                       
      SUBROUTINE NORMAL(X,V)                                            
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      DIMENSION X(3),V(3)                                               
      XS=XSIZE(X)
      DO 1 J=1,3                                                        
 1    V(J)=X(J)/XS                                                      
      RETURN                                                            
      END                                                               
C                                                                       
C  FUNCTION SIZE - LENGTH OF A 3D VECTOR                                
C                                                                       
      DOUBLE PRECISION FUNCTION XSIZE(X)
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      DIMENSION X(3)                                                    
      S=X(1)*X(1)+X(2)*X(2)+X(3)*X(3)                                   
      XSIZE=DSQRT(S)
      RETURN                                                            
      END                                                               
