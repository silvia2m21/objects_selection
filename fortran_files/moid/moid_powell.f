	SUBROUTINE MOID(RMIN)
C
C	Find minimum distance between two orbits (RMIN in AU)
C	We use 1 and 2 subscripts for the two orbits.
C	The orbital elements for the two bodies are passed in COMMOM ORB1 and ORB2
C	   APn - Argument of perihelion (deg)
C	   ANn - Longitude of ascending node (deg)
C	   AIn - Inclination (deg)
C	   AEn - Eccentricity
C	   AAn - Semi-major axis (AU)
C
	IMPLICIT NONE
	REAL*8 one,zero,PI,DEGRAD,ftol,fret,ftolori
	real*8 V1(2),V2(2),V3(2),V4(2),xi(2,2),rmin,rmini(4)
	INTEGER n,np,iter,ITMAX,irep,i
	PARAMETER (ITMAX=200)
	one=1.d0
	zero=0.d0
	PI=datan(one)*4.d0
	DEGRAD=PI/180.d0
	ftolori=1.d-6
	ftol=ftolori
C---	Quantities K, L, M, and N
	CALL KLMN
	irep=0
	V1(1)=pi/2.
	V1(2)=pi/2.
	xi(1,1)=one
	xi(1,2)=zero
	xi(2,1)=zero
	xi(2,2)=one
	n=2
	np=n
 1	call powell(V1,xi,n,np,ftol,iter,fret)
	if (iter.eq.ITMAX) then
	   if (ftol.lt.1.e-3) then
	      irep=irep+1
	      V1(1)=PI/real(irep)
	      V1(2)=PI/real(irep)
	      xi(1,1)=one
	      xi(1,2)=zero
	      xi(2,1)=zero
	      xi(2,2)=one
	      ftol=ftol*10.d0
	      goto 1
	   else
	      print *,'no good solution'
	   endif
	endif
	ftol=ftolori
	rmini(1)=sqrt(fret*2.d0)
c	print *,'1',iter,V1,rmini(1)

c	V2(1)=mod(V1(1)+pi,2.*pi)
c	V2(2)=mod(V1(2)+pi,2.*pi)
	V2(1)=3.*pi/2.
	V2(2)=pi/2.
	xi(1,1)=zero
	xi(1,2)=one
	xi(2,1)=one
	xi(2,2)=zero
	n=2
	np=n
    	call powell(V2,xi,n,np,ftol,iter,fret)
	rmini(2)=sqrt(fret*2.d0)
c	print *,'2',iter,V2,rmini(2)

c	V3(1)=V1(1)
c	V3(2)=mod(2.*pi-V1(2),2.*pi)
	V3(1)=pi/2.
	V3(2)=3.*pi/2.
	xi(1,1)=zero
	xi(1,2)=one
	xi(2,1)=one
	xi(2,2)=zero
	n=2
	np=n
    	call powell(V3,xi,n,np,ftol,iter,fret)
	rmini(3)=sqrt(fret*2.d0)
c	print *,'3',iter,V3,rmini(3)

c	V4(1)=V2(1)
c	V4(2)=mod(2.*pi-V2(2),2.*pi)
	V4(1)=3.*pi/2.
	V4(2)=3.*pi/2.
	xi(1,1)=one
	xi(1,2)=zero
	xi(2,1)=zero
	xi(2,2)=one
	n=2
	np=n
    	call powell(V4,xi,n,np,ftol,iter,fret)
	rmini(4)=sqrt(fret*2.d0)
c	print *,'4',iter,V4,rmini(4)
	rmin=rmini(1)
	do i=2,4
	   if (rmini(i).lt.rmin) rmin=rmini(i)
	enddo
c	print *,'rmin',rmin
	RETURN
	END
