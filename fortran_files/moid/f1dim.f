      FUNCTION f1dim(x)
      IMPLICIT NONE
      INTEGER NMAX
      REAL*8 f1dim,func,x
      PARAMETER (NMAX=50)
CU    USES func
      INTEGER j,ncom
      REAL*8 pcom(NMAX),xicom(NMAX),xt(NMAX)
      COMMON /f1com/ pcom,xicom,ncom
      do 11 j=1,ncom
        xt(j)=pcom(j)+x*xicom(j)
11    continue
      f1dim=func(xt)
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software =$j!925N.
