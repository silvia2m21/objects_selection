C
C-----------------------------------------------------------------------------
C
C  SUBROUTINE PRVEC - VECTOR PRODUCT OF A AND B IS C                    
C                                                                       
      SUBROUTINE PRVEC(A,B,C)                                           
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      DIMENSION A(3),B(3),C(3)                                          
      C(1)=A(2)*B(3)-A(3)*B(2)                                          
      C(2)=A(3)*B(1)-A(1)*B(3)                                          
      C(3)=A(1)*B(2)-A(2)*B(1)                                          
      RETURN                                                            
      END                                                               
