      program ext_jpl_com
c read data from file ELEMENTS.comet from JPL
                                                                     
      implicit real*8 (a-h,o-z)
      character*1 type,slash,name*38
      character*1 nameshort*15,name2short*15,namedone*15
      real*4 day
      real*8 inc,n
      data inum/10000/
      gauss=0.01720209895d0
      pi=datan(1.d0)*4.d0
      open(1,file='ELEMENTS.COMET',status='old')
      open(2,file='elem_jpl',status='unknown')
      numberdone=0
      namedone=''
      inumn=500
      read (1,99) type
      read (1,99) type
 99   format(a1)
 1    read (1,100,end=999,err=2) number,type,slash,name,iepoch,q,ecc,
     #     inc,somega,bomega,iy,im,day
 100  format(i3,a1,a1,a38,1x,i7,1x,f11.8,1x,f10.8,1x,3(f9.5,1x),i4,
     #     i2,f8.5)
      goto 3
 2    backspace 1
      read (1,110,end=999,err=999) number,type,slash,name,iepoch,q,ecc,
     #     inc,somega,bomega,iy,im,day
 110  format(i2,a1,a1,a38,1x,i7,1x,f11.8,1x,f10.8,1x,3(f9.5,1x),i4,
     #     i2,f8.5)
 3    continue
c      if (slash.eq.'-')
cNum  Name                                     Epoch      q           e        i         w        Node          Tp       Ref
c------------------------------------------- ------- ----------- ---------- --------- --------- --------- -------------- ------------
c  1P/Halley                                   49400  0.58597811 0.96714291 162.26269 111.33249  58.42008 19860205.89532 JPL J863/77
c 73P/Schwassmann-Wachmann 3-B                 53880  0.93911104 0.69328552  11.39695 198.79945  69.89248 20060607.92443 JPL K061/24
c   D/1993 F2-A (Shoemaker-Levy 9)             49480  5.38056310 0.21620917   6.00329 354.89352 220.53766 19940324.10320 JPL A38

      if (type.eq.'C'.and.ecc.ge.1.d0) goto 1
      axis=q/(1.d0-ecc)
      period=axis**1.5
      if (period.gt.200) goto 1
      if (number.eq.numberdone) goto 1
      if (number.eq.0) then
         lenname=len(name)
         ichar1=index(name,'(')
         if (ichar1.eq.0) then
            ichar1=lenname
         else
            ichar1=ichar1-1
         end if
         nameshort=name(1:ichar1)
         ichar2=index(nameshort,'-')
         if (ichar2.eq.0) then
            ichar2=ichar1
         else
            ichar2=ichar2-1
         end if
         name2short=nameshort(1:ichar2)
c         print *,lenaname,ichar1,ichar2,name,nameshort,name2short
c         if (name2short.eq.namedone) print *,name2short,namedone
         if (name2short.eq.namedone) goto 1
      end if
      epoch=2400000.5+real(iepoch)
      call julian(iy,im,day,Tp) 
      n=gauss/(axis*dsqrt(axis))*180./pi
      anom=n*(epoch-Tp)
      anom=dmod(anom,360.d0)
      if (anom.lt.0.) anom=anom+360.0
c      do i=1,18
c         if (name(i:i).eq.' ') then
c            if (name(i+1:i+1).eq.' ') then 
c               goto 11
c            else
c               name(i:i)='_'
c            end if
c         else if (name(i:i).eq.'-'.or.name(i:i).eq.'''') then
c               name(i:i)='_'
c         end if
c      end do            
c 11   continue
      if (number.gt.0) then
         inum=number
         if (axis.lt.100.) then
            write(2,101) number,axis,ecc,inc,bomega,somega,anom,epoch,
     #           number,name
         else
            write(2,102) number,axis,ecc,inc,bomega,somega,anom,epoch,
     #           number,name
         end if
      else
         inumn=inumn+1
         number=inumn
         if (axis.lt.100.) then
            write(2,103) number,axis,ecc,inc,bomega,somega,anom,epoch,
     #           name
         else
            write(2,104) number,axis,ecc,inc,bomega,somega,anom,epoch,
     #           name
         end if
         namedone=name2short
      end if
      numberdone=number
      goto 1
 101  format(i6,2x,2f10.7,4f10.5,f10.1,9x,i6,1x,a32)
 102  format(i6,2x,f10.6,f10.7,4f10.5,f10.1,9x,i6,1x,a32)      
 103  format(i6,2x,2f10.7,4f10.5,f10.1,16x,a32)
 104  format(i6,2x,f10.6,f10.7,4f10.5,f10.1,16x,a32)      
 999  continue 
      close(1)
      close(2)
      end

c
      subroutine julian(year,month,day,jd)
      dimension man(12)
      real*8 centur(10),jd 
      real*4 day
      integer year
      logical add,march,greg
      save centur,man
      data centur/2122831.5d0,2159356.5d0,2195881.5d0,2232406.5d0,
     a 2268931.5d0,2305446.5d0,2341971.5d0,2378495.5d0,2415019.5d0,
     b 2451543.5d0/
      data man/0,31,59,90,120,151,181,212,243,273,304,334/   
      jd=0.d0
      iy=year
      greg=.false.
    1 if(iy.lt.2000) go to 10
      jd=jd+1.46097d5
      iy=iy-400
      go to 1
   10 if(iy.ge.1100) go to 20
      jd=jd-1.461d5
      iy=iy+400
      go to 10
   20 icen=iy/100
      jd=jd+centur(icen-10)
      iy=iy-icen*100
      march=month.gt.2
      add=march.or.iy.gt.0
      nfeb=iy
      if(march) nfeb=nfeb+1
      leap=(nfeb-1)/4
      if(add.and.icen.le.16) leap=leap+1
      id=iy*365+leap+man(month)
      jd=jd+dfloat(id)+day
      if(icen.ne.15) go to 40
      if(iy-82) 40,35,39
   35 if(month-10) 40,36,39
   36 if(day.lt.15.d0) go to 40
   39 greg=.true.
   40 if(greg) jd=jd-10.d0
      return
      end
 
