c
c**********************************************************************
c
c     jddate         version 86-12-12
c
c**********************************************************************
c
      implicit real*8(a-h,o-z)
c
 10    continue
c      write(6,100)
c 100  format(5x,'which kind of conversion do you want?'
c     *///5x,'year,month,day ==> julian date : enter  1'
c     */5x,'julian date ==> year,month,day : enter  2')
      read(5,*) icode
      if(icode.lt.1 .or. icode.gt.2) goto 10
c
c     alternative 1
c
      if(icode.ne.1) goto 20
c      write(6,200)
c 200  format(2x,' give : year,month,day    (e.g. 1978,10,15.2)'//)
      read(5,*) iyear,month,day
c
      call julian(iyear,month,day,xjd)
c
      write(6,201) xjd
 201  format(f20.5)
      goto 900
c
c     alternative 2
c
 20   continue
c      write(6,202)
c 202  format(2x,' give jd'//)
      read(5,*) xjd
c
      call nailuj(xjd,iyear,month,day)
c
      write(6,203) iyear,month,day
 203  format(i6,i3,f9.5)
c
 900  continue
c
  99  stop 
      end
c
c----------------------------------------------------------------------
c
      subroutine julian(year,month,day,jd)
      dimension man(12)
      real*8 centur(10),day,jd
      integer year
      logical add,march,greg
      data centur/2122831.5d0,2159356.5d0,2195881.5d0,2232406.5d0,
     a 2268931.5d0,2305446.5d0,2341971.5d0,2378495.5d0,2415019.5d0,
     b 2451543.5d0/
      data man/0,31,59,90,120,151,181,212,243,273,304,334/
      jd=0.d0
      iy=year
      greg=.false.
    1 if(iy.lt.2000) go to 10
      jd=jd+1.46097d5
      iy=iy-400
      go to 1
   10 if(iy.ge.1100) go to 20
      jd=jd-1.461d5
      iy=iy+400
      go to 10
   20 icen=iy/100
      jd=jd+centur(icen-10)
      iy=iy-icen*100
      march=month.gt.2
      add=march.or.iy.gt.0
      nfeb=iy
      if(march) nfeb=nfeb+1
      leap=(nfeb-1)/4
      if(add.and.icen.le.16) leap=leap+1
      id=iy*365+leap+man(month)
      jd=jd+dfloat(id)+day
      if(icen.ne.15) go to 40
      if(iy-82) 40,35,39
   35 if(month-10) 40,36,39
   36 if(day.lt.15.d0) go to 40
   39 greg=.true.
   40 if(greg) jd=jd-10.d0
      return
      end
c
c------------------------------------------------------------------
c
      subroutine nailuj(jd,year,month,day)
      dimension man(12)
      real*8 centur(10),jd,day,test
      integer year
      logical greg,leap
      data centur/2122832.5d0,2159357.5d0,2195882.5d0,2232407.5d0,
     a 2268932.5d0,2305447.5d0,2341972.5d0,2378496.5d0,2415020.5d0,
     b 2451544.5d0/
      data man/0,31,59,90,120,151,181,212,243,273,304,334/
c     do 1 i=1,10
c   1 centur(i)=centur(i)+1.d0
      leap=.false.
      icen=0
    5 if(jd.ge.centur(1)) go to 10
      jd=jd+1.461d5
      icen=icen-4
      go to 5
   10 if(jd.lt.centur(10)) go to 20
      jd=jd-1.46097d5
      icen=icen+4
      go to 10
   20 do 25 i=1,9
      if(jd.ge.centur(i).and.jd.lt.centur(i+1)) go to 30
   25 continue
   30 icen=icen+i+10
      day=jd-centur(i)
      iy=0
   35 test=3.65d2
      greg=iy.eq.0.and.i.gt.6
      if(icen.eq.15.and.iy.eq.82) test=test-10.d0
      if(mod(iy,4).eq.0) test=test+1.d0
      if(greg) test=test-1.d0
      if(day.lt.test) go to 40
      day=day-test
      iy=iy+1
      go to 35
   40 year=icen*100+iy
      month=1
      if(day.lt.dfloat(man(2))) go to 50
      month=2
      leap=test.gt.3.65d2
      do 45 i=3,12
      test=dfloat(man(i))
      if(leap) test=test+1.d0
      if(year.eq.1582.and.i.gt.10) test=test-10.d0
      if(day.lt.test) go to 50
   45 month=month+1
   50 day=day-dfloat(man(month))+1.d0
      if(leap.and.month.gt.2) day=day-1.d0
      if(year.ne.1582) go to 60
      if(month-10) 60,55,59
   55 if(day.lt.5.d0) go to 60
   59 day=day+10.d0
   60 return
      end
