	SUBROUTINE MOID(RMIN)
C
C	Find minimum distance between two orbits (RMIN in AU)
C	We use 1 and 2 subscripts for the two orbits.
C	The orbital elements for the two bodies are passed in COMMOM ORB1 and ORB2
C	   APn - Argument of perihelion (deg)
C	   ANn - Longitude of ascending node (deg)
C	   AIn - Inclination (deg)
C	   AEn - Eccentricity
C	   AAn - Semi-major axis (AU)
C
	IMPLICIT NONE
	REAL*8 one,zero,PI,DEGRAD,ftol,fret,ftolori
	real*8 V1(2),V2(2),V3(2),V4(2),xi(2,2),rmin,rmini(4)
	INTEGER n,np,iter,ITMAX,irep,i
	PARAMETER (ITMAX=200)
	one=1.d0
	zero=0.d0
	PI=datan(one)*4.d0
	DEGRAD=PI/180.d0
	ftolori=1.d-6
	ftol=ftolori
C---	Quantities K, L, M, and N
	CALL KLMN
	irep=0
	V1(1)=pi/2.
	V1(2)=pi/2.
	xi(1,1)=one
	xi(1,2)=zero
	xi(2,1)=zero
	xi(2,2)=one
	n=2
	np=n
 1	call powell(V1,xi,n,np,ftol,iter,fret)
	if (iter.eq.ITMAX) then
	   if (ftol.lt.1.e-3) then
	      irep=irep+1
	      V1(1)=PI/real(irep)
	      V1(2)=PI/real(irep)
	      xi(1,1)=one
	      xi(1,2)=zero
	      xi(2,1)=zero
	      xi(2,2)=one
	      ftol=ftol*10.d0
	      goto 1
	   else
	      print *,'no good solution'
	   endif
	endif
	ftol=ftolori
	rmini(1)=sqrt(fret*2.d0)
c	print *,'1',iter,V1,rmini(1)

c	V2(1)=mod(V1(1)+pi,2.*pi)
c	V2(2)=mod(V1(2)+pi,2.*pi)
	V2(1)=3.*pi/2.
	V2(2)=pi/2.
	xi(1,1)=zero
	xi(1,2)=one
	xi(2,1)=one
	xi(2,2)=zero
	n=2
	np=n
    	call powell(V2,xi,n,np,ftol,iter,fret)
	rmini(2)=sqrt(fret*2.d0)
c	print *,'2',iter,V2,rmini(2)

c	V3(1)=V1(1)
c	V3(2)=mod(2.*pi-V1(2),2.*pi)
	V3(1)=pi/2.
	V3(2)=3.*pi/2.
	xi(1,1)=zero
	xi(1,2)=one
	xi(2,1)=one
	xi(2,2)=zero
	n=2
	np=n
    	call powell(V3,xi,n,np,ftol,iter,fret)
	rmini(3)=sqrt(fret*2.d0)
c	print *,'3',iter,V3,rmini(3)

c	V4(1)=V2(1)
c	V4(2)=mod(2.*pi-V2(2),2.*pi)
	V4(1)=3.*pi/2.
	V4(2)=3.*pi/2.
	xi(1,1)=one
	xi(1,2)=zero
	xi(2,1)=zero
	xi(2,2)=one
	n=2
	np=n
    	call powell(V4,xi,n,np,ftol,iter,fret)
	rmini(4)=sqrt(fret*2.d0)
c	print *,'4',iter,V4,rmini(4)
	rmin=rmini(1)
	do i=2,4
	   if (rmini(i).lt.rmin) rmin=rmini(i)
	enddo
c	print *,'rmin',rmin
	RETURN
	END
      SUBROUTINE powell(p,xi,n,np,ftol,iter,fret)
      IMPLICIT NONE
      INTEGER iter,n,np,NMAX,ITMAX
      REAL*8 fret,ftol,p(np),xi(np,np),func
      EXTERNAL func
      PARAMETER (NMAX=20,ITMAX=200)
CU    USES func,linmin
      INTEGER i,ibig,j
      REAL*8 del,fp,fptt,t,pt(NMAX),ptt(NMAX),xit(NMAX)
      fret=func(p)
c      print *,'cero',(p(j),j=1,n),((xi(j,i),j=1,n) ,i=1,n)
      do 11 j=1,n
        pt(j)=p(j)
11    continue
      iter=0
1     iter=iter+1
      fp=fret
      ibig=0
      del=0.d0
      do 13 i=1,n
        do 12 j=1,n
          xit(j)=xi(j,i)
12      continue
        fptt=fret
        call linmin(p,xit,n,fret)
        if(abs(fptt-fret).gt.del)then
          del=abs(fptt-fret)
          ibig=i
        endif
13    continue
      if(2.d0*abs(fp-fret).le.ftol*(abs(fp)+abs(fret)))return
      if(iter.eq.ITMAX) then
c        print *,'powell exceeding maximum iterations'
        return
      endif
      do 14 j=1,n
        ptt(j)=2.d0*p(j)-pt(j)
        xit(j)=p(j)-pt(j)
        pt(j)=p(j)
14    continue
      fptt=func(ptt)
      if(fptt.ge.fp)goto 1
      t=2.d0*(fp-2.d0*fret+fptt)*(fp-fret-del)**2-del*(fp-fptt)**2
      if(t.ge.0.d0)goto 1
      call linmin(p,xit,n,fret)
      do 15 j=1,n
        xi(j,ibig)=xi(j,n)
        xi(j,n)=xit(j)
15    continue
      goto 1
      END
C  (C) Copr. 1986-92 Numerical Recipes Software =$j!925N.
      SUBROUTINE linmin(p,xi,n,fret)
      IMPLICIT NONE
      INTEGER n,NMAX
      REAL*8 fret,p(n),xi(n),TOL
      PARAMETER (NMAX=50,TOL=1.e-4)
CU    USES brent,f1dim,mnbrak
      INTEGER j,ncom
      REAL*8 ax,bx,fa,fb,fx,xmin,xx,pcom(NMAX),xicom(NMAX),brent
      COMMON /f1com/ pcom,xicom,ncom
      REAL*8 f1dim
      EXTERNAL f1dim
      ncom=n
      do 11 j=1,n
        pcom(j)=p(j)
        xicom(j)=xi(j)
11    continue
      ax=0.
      xx=1.
      call mnbrak(ax,xx,bx,fa,fx,fb,f1dim)
      fret=brent(ax,xx,bx,f1dim,TOL,xmin)
      do 12 j=1,n
        xi(j)=xmin*xi(j)
        p(j)=p(j)+xi(j)
12    continue
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software =$j!925N.

      FUNCTION f1dim(x)
      IMPLICIT NONE
      INTEGER NMAX
      REAL*8 f1dim,func,x
      PARAMETER (NMAX=50)
CU    USES func
      INTEGER j,ncom
      REAL*8 pcom(NMAX),xicom(NMAX),xt(NMAX)
      COMMON /f1com/ pcom,xicom,ncom
      do 11 j=1,ncom
        xt(j)=pcom(j)+x*xicom(j)
11    continue
      f1dim=func(xt)
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software =$j!925N.
      SUBROUTINE mnbrak(ax,bx,cx,fa,fb,fc,func)
      IMPLICIT NONE
      REAL*8 ax,bx,cx,fa,fb,fc,func,GOLD,GLIMIT,TINY
      EXTERNAL func
      PARAMETER (GOLD=1.618034, GLIMIT=100., TINY=1.e-20)
      REAL*8 dum,fu,q,r,u,ulim
      fa=func(ax)
      fb=func(bx)
      if(fb.gt.fa)then
        dum=ax
        ax=bx
        bx=dum
        dum=fb
        fb=fa
        fa=dum
      endif
      cx=bx+GOLD*(bx-ax)
      fc=func(cx)
1     if(fb.ge.fc)then
        r=(bx-ax)*(fb-fc)
        q=(bx-cx)*(fb-fa)
        u=bx-((bx-cx)*q-(bx-ax)*r)/(2.*sign(max(abs(q-r),TINY),q-r))
        ulim=bx+GLIMIT*(cx-bx)
        if((bx-u)*(u-cx).gt.0.)then
          fu=func(u)
          if(fu.lt.fc)then
            ax=bx
            fa=fb
            bx=u
            fb=fu
            return
          else if(fu.gt.fb)then
            cx=u
            fc=fu
            return
          endif
          u=cx+GOLD*(cx-bx)
          fu=func(u)
        else if((cx-u)*(u-ulim).gt.0.)then
          fu=func(u)
          if(fu.lt.fc)then
            bx=cx
            cx=u
            u=cx+GOLD*(cx-bx)
            fb=fc
            fc=fu
            fu=func(u)
          endif
        else if((u-ulim)*(ulim-cx).ge.0.)then
          u=ulim
          fu=func(u)
        else
          u=cx+GOLD*(cx-bx)
          fu=func(u)
        endif
        ax=bx
        bx=cx
        cx=u
        fa=fb
        fb=fc
        fc=fu
        goto 1
      endif
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software =$j!925N.
      REAL*8 FUNCTION brent(ax,bx,cx,f,tol,xmin)
      IMPLICIT NONE
      INTEGER ITMAX
      REAL*8 ax,bx,cx,tol,xmin,f,CGOLD,ZEPS
      EXTERNAL f
      PARAMETER (ITMAX=100,CGOLD=.3819660,ZEPS=1.0e-10)
      INTEGER iter
      REAL*8 a,b,d,e,etemp,fu,fv,fw,fx,p,q,r,tol1,tol2,u,v,w,x,xm
      a=min(ax,cx)
      b=max(ax,cx)
      v=bx
      w=v
      x=v
      e=0.
      fx=f(x)
      fv=fx
      fw=fx
      do 11 iter=1,ITMAX
        xm=0.5*(a+b)
        tol1=tol*abs(x)+ZEPS
        tol2=2.*tol1
        if(abs(x-xm).le.(tol2-.5*(b-a))) goto 3
        if(abs(e).gt.tol1) then
          r=(x-w)*(fx-fv)
          q=(x-v)*(fx-fw)
          p=(x-v)*q-(x-w)*r
          q=2.*(q-r)
          if(q.gt.0.) p=-p
          q=abs(q)
          etemp=e
          e=d
          if(abs(p).ge.abs(.5*q*etemp).or.p.le.q*(a-x).or.p.ge.q*(b-x)) 
     *goto 1
          d=p/q
          u=x+d
          if(u-a.lt.tol2 .or. b-u.lt.tol2) d=sign(tol1,xm-x)
          goto 2
        endif
1       if(x.ge.xm) then
          e=a-x
        else
          e=b-x
        endif
        d=CGOLD*e
2       if(abs(d).ge.tol1) then
          u=x+d
        else
          u=x+sign(tol1,d)
        endif
        fu=f(u)
        if(fu.le.fx) then
          if(u.ge.x) then
            a=x
          else
            b=x
          endif
          v=w
          fv=fw
          w=x
          fw=fx
          x=u
          fx=fu
        else
          if(u.lt.x) then
            a=u
          else
            b=u
          endif
          if(fu.le.fw .or. w.eq.x) then
            v=w
            fv=fw
            w=u
            fw=fu
          else if(fu.le.fv .or. v.eq.x .or. v.eq.w) then
            v=u
            fv=fu
          endif
        endif
11    continue
c      pause 'brent exceed maximum iterations'
3     xmin=x
      brent=fx
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software =$j!925N.
      real*8 function func(V)
      implicit none
      real*8 V(2),h1(3,3),h2(3,3),u1(3),u2(3)
      real*8 V1,V2,RV1(3),RV2(3),diff(3)
      real*8 R1,R2,X1,Y1,X2,Y2
      real*8 zero
      real*8 prscal
      integer i
      COMMON/RXY/R1,R2,X1,Y1,X2,Y2
      zero=0.d0
      V1=V(1)
      V2=V(2)
      call R1R2(V1,V2)
      u1(1)=X1
      u1(2)=Y1
      u1(3)=zero
      u2(1)=X2
      u2(2)=Y2
      u2(3)=zero
      call h1h2(h1,h2)
      call prodmv(RV1,h1,u1)
      call prodmv(RV2,h2,u2)
      do i=1,3
         diff(i)=RV1(i)-RV2(i)
      enddo
      func=prscal(diff,diff)/2.d0
c      print *,sqrt(func*2.d0)
      return
      end

      subroutine dfunc(V,DF)
      implicit none
      REAL*8 V(2),DF(2)
      REAL*8 R1,R2,X1,Y1,X2,Y2
      REAL*8 P1,P2,K,L,M,N
      REAL*8 AP1,AN1,AI1,AE1,AA1
      REAL*8 AP2,AN2,AI2,AE2,AA2
      COMMON/RXY/R1,R2,X1,Y1,X2,Y2
      COMMON/DATA/P1,P2,K,L,M,N
      COMMON/ORB1/AP1,AN1,AI1,AE1,AA1
      COMMON/ORB2/AP2,AN2,AI2,AE2,AA2
      DF(1)=R1/P1*(AE1*R1*Y1+Y1*(K*X2+M*Y2)-(AE1*R1+X1)*(L*X2+N*Y2))
      DF(2)=R2/P2*(AE2*R2*Y2+Y2*(K*X1+L*Y1)-(AE2*R2+X2)*(M*X1+N*Y1))
      return
      end

      SUBROUTINE R1R2(V1,V2)
      implicit none
      REAL*8 V1,V2
      REAL*8 R1,R2,X1,Y1,X2,Y2
      REAL*8 AP1,AN1,AI1,AE1,AA1
      REAL*8 AP2,AN2,AI2,AE2,AA2
      REAL*8 P1,P2,K,L,M,N
      COMMON/RXY/R1,R2,X1,Y1,X2,Y2
      COMMON/ORB1/AP1,AN1,AI1,AE1,AA1
      COMMON/ORB2/AP2,AN2,AI2,AE2,AA2
      COMMON/DATA/P1,P2,K,L,M,N
      R1=P1/(1.d0+AE1*COS(V1))
      R2=P2/(1.d0+AE2*COS(V2))
      X1=R1*COS(V1)
      Y1=R1*SIN(V1)
      X2=R2*COS(V2)
      Y2=R2*SIN(V2)
      return
      end

      subroutine h1h2(h1,h2)
      implicit none
      real*8 h1(3,3),h2(3,3),h3(3,3)
      real*8 h11(3,3),h12(3,3),h21(3,3),h22(3,3),h23(3,3)
      REAL*8 CAP1,SAP1,CAI1,SAI1
      REAL*8 CAP2,SAP2,CAI2,SAI2
      REAL*8 CDAN,SDAN
      real*8 one,zero
      COMMON/ANG1/CAP1,SAP1,CAI1,SAI1
      COMMON/ANG2/CAP2,SAP2,CAI2,SAI2
      COMMON/DANG/CDAN,SDAN
      one=1.d0
      zero=0.d0
      h11(1,1)=one
      h11(1,2)=zero
      h11(1,3)=zero
      h11(2,1)=zero
      h11(2,2)=CAI1
      h11(2,3)=SAI1
      h11(3,1)=zero
      h11(3,2)=-SAI1
      h11(3,3)=CAI1

      h12(3,3)=one
      h12(3,2)=zero
      h12(3,1)=zero
      h12(2,3)=zero
      h12(2,2)=CAP1
      h12(2,1)=SAP1
      h12(1,3)=zero
      h12(1,2)=-SAP1
      h12(1,1)=CAP1

      call prodmm(h1,h11,h12)

      h21(3,3)=one
      h21(3,2)=zero
      h21(3,1)=zero
      h21(2,3)=zero
      h21(2,2)=CDAN
      h21(2,1)=SDAN
      h21(1,3)=zero
      h21(1,2)=-SDAN
      h21(1,1)=CDAN

      h22(1,1)=one
      h22(1,2)=zero
      h22(1,3)=zero
      h22(2,1)=zero
      h22(2,2)=CAI2
      h22(2,3)=SAI2
      h22(3,1)=zero
      h22(3,2)=-SAI2
      h22(3,3)=CAI2

      h23(3,3)=one
      h23(3,2)=zero
      h23(3,1)=zero
      h23(2,3)=zero
      h23(2,2)=CAP2
      h23(2,1)=SAP2
      h23(1,3)=zero
      h23(1,2)=-SAP2
      h23(1,1)=CAP2

      call prodmm(h3,h21,h22)
      call prodmm(h2,h3,h23)

      return
      end

      
	SUBROUTINE KLMN
C
C	Compute K,L,M,N from Sitarski and the upper and lower case P's and Q's
C
	IMPLICIT NONE
	REAL*8 AP1,AN1,AI1,AE1,AA1
	REAL*8 AP2,AN2,AI2,AE2,AA2
	REAL*8 PX1,QX1,PY1,QY1,PZ1,QZ1
	REAL*8 PX2,QX2,PY2,QY2,PZ2,QZ2
        REAL*8 CAP1,SAP1,CAI1,SAI1
        REAL*8 CAP2,SAP2,CAI2,SAI2
        REAL*8 CDAN,SDAN
	REAL*8 P1,P2,K,L,M,N
        REAL*8 R1,R2,X1,Y1,X2,Y2
        REAL*8 DEGRAD,Q1,Q2
	REAL*8 AP1R,AN1R,AI1R
	REAL*8 AP2R,AN2R,AI2R

	COMMON/ORB1/AP1,AN1,AI1,AE1,AA1
	COMMON/ORB2/AP2,AN2,AI2,AE2,AA2
	COMMON/VEC1/PX1,QX1,PY1,QY1,PZ1,QZ1
	COMMON/VEC2/PX2,QX2,PY2,QY2,PZ2,QZ2
        COMMON/ANG1/CAP1,SAP1,CAI1,SAI1
        COMMON/ANG2/CAP2,SAP2,CAI2,SAI2
        COMMON/DANG/CDAN,SDAN
	COMMON/DATA/P1,P2,K,L,M,N
        COMMON/RXY/R1,R2,X1,Y1,X2,Y2
	DEGRAD=datan(1.d0)*4.d0/180.D0
	Q1=(1.-AE1)*AA1
	Q2=(1.-AE2)*AA2
	P1=(1.+AE1)*Q1
	P2=(1.+AE2)*Q2
C---	Compute the capital P, Q values and save in VEC1 and VEC2
	AN1R=AN1*DEGRAD
	AP1R=AP1*DEGRAD
	AI1R=AI1*DEGRAD
	SAP1=SIN(AP1R)
	CAP1=COS(AP1R)
	SAI1=SIN(AI1R)
	CAI1=COS(AI1R)
	PX1=+CAP1
	QX1=-SAP1
	PY1=SAP1*CAI1
	QY1=CAP1*CAI1
	PZ1=SAP1*SAI1
	QZ1=CAP1*SAI1
	AN2R=AN2*DEGRAD
	AP2R=AP2*DEGRAD
	AI2R=AI2*DEGRAD
	SAP2=SIN(AP2R)
	CAP2=COS(AP2R)
	SAI2=SIN(AI2R)
	CAI2=COS(AI2R)
C---    And the lower case p,q values as well as K, L, M, N
	SDAN=SIN(AN2R-AN1R)
	CDAN=COS(AN2R-AN1R)
	PX2=CAP2*CDAN-SAP2*CAI2*SDAN
	PY2=CAP2*SDAN+SAP2*CAI2*CDAN
	PZ2=SAP2*SAI2
	QX2=-SAP2*CDAN-CAP2*CAI2*SDAN
	QY2=-SAP2*SDAN+CAP2*CAI2*CDAN
	QZ2=+CAP2*SAI2
	K=PX1*PX2+PY1*PY2+PZ1*PZ2
	L=QX1*PX2+QY1*PY2+QZ1*PZ2
	M=PX1*QX2+PY1*QY2+PZ1*QZ2
	N=QX1*QX2+QY1*QY2+QZ1*QZ2
	RETURN
	END

* Author: Mario Carpino (carpino@brera.mi.astro.it)
* Version: June 5, 1996
*
*  ***************************************************************
*  *                                                             *
*  *                         P R O D M M                         *
*  *                                                             *
*  *                 Product of two 3x3 matrices                 *
*  *                           A = BC                            *
*  *                                                             *
*  ***************************************************************
*
      subroutine prodmm(a,b,c)
      implicit none

      double precision a(3,3),b(3,3),c(3,3),s
      integer j,k,l

      do 2 j=1,3
      do 2 k=1,3
      s=0.d0
      do 1 l=1,3
 1    s=s+b(j,l)*c(l,k)
 2    a(j,k)=s

      end
* Author: Mario Carpino (carpino@brera.mi.astro.it)
* Version: June 5, 1996
*
*  ***************************************************************
*  *                                                             *
*  *                         P R O D M V                         *
*  *                                                             *
*  *            Product of a 3x3 matrix by a 3-D vector          *
*  *                           y = Ax                            *
*  *                                                             *
*  ***************************************************************
*
      SUBROUTINE prodmv(y,a,x)
      IMPLICIT NONE
      DOUBLE PRECISION x(3),y(3),a(3,3),s
      INTEGER j,l

      DO 2 j=1,3
      s=0.d0
      DO 1 l=1,3
 1    s=s+a(j,l)*x(l)
 2    y(j)=s

      END
c
      double precision function prscal(a,b)
c
c    Computes the scalar product of two 3d vectors
c
      double precision a(3),b(3)
      prscal=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
      return
      end
C
C-----------------------------------------------------------------------------
C
C  SUBROUTINE PRVEC - VECTOR PRODUCT OF A AND B IS C                    
C                                                                       
      SUBROUTINE PRVEC(A,B,C)                                           
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      DIMENSION A(3),B(3),C(3)                                          
      C(1)=A(2)*B(3)-A(3)*B(2)                                          
      C(2)=A(3)*B(1)-A(1)*B(3)                                          
      C(3)=A(1)*B(2)-A(2)*B(1)                                          
      RETURN                                                            
      END                                                               
C
C-----------------------------------------------------------------------------
C
C SUBROUTINE NORMAL COMPUTES THE UNIT VECTOR V FOR A GIVEN VECTOR X     
C                                                                       
      SUBROUTINE NORMAL(X,V)                                            
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      DIMENSION X(3),V(3)                                               
      XS=XSIZE(X)
      DO 1 J=1,3                                                        
 1    V(J)=X(J)/XS                                                      
      RETURN                                                            
      END                                                               
C                                                                       
C  FUNCTION SIZE - LENGTH OF A 3D VECTOR                                
C                                                                       
      DOUBLE PRECISION FUNCTION XSIZE(X)
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      DIMENSION X(3)                                                    
      S=X(1)*X(1)+X(2)*X(2)+X(3)*X(3)                                   
      XSIZE=DSQRT(S)
      RETURN                                                            
      END                                                               
