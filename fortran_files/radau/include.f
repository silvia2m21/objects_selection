      Character*40 Restartfile,
     &             Plsysfile,
     &             Approachfile,
     &             Osculatefile,
     &             Sysoutinit,
     &             Sysoutintegfile,
     &             Startvalfile,
     &             Elementfile,
     &             Ngffile
      Character*10 OutputFile

      Parameter (Restartfile     = 'restart',
     &           Plsysfile       = 'plsys',
     &           Approachfile    = 'approach',
     &           Osculatefile    = 'osculate',
     &           Sysoutinit      = 'sysoutinit',
     &           Sysoutintegfile = 'sysoutinteg',
     &           Startvalfile    = 'startval',
     &           Elementfile     = 'elements',
     &           Ngffile         = 'ngf',
     &           OutputFile      = 'output')
