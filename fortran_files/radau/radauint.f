c
c     *****************************************************************
c
c     program name:  radauint     version:  88-10-13
c
c     *****************************************************************
c
      implicit real*8 (a-h,o-z)
c

      include 'include.f'
c
      parameter (ncom=1000)
      parameter (npl=9)
      parameter (ntot=ncom+npl)
      parameter (ntot3=ntot*3)

      character*20 name
      integer*4 type_of_output
c
      common /dist/ time,dlim(ntot),axis(ntot),name(ntot)
      common /body/ zmass,bmass(ntot),nbody,nplan
      common /timex/ Epoch,Epoch0
c
      dimension x(3),y(3),cor(ntot3),vel(ntot3),el(6),amass(ntot),
     *          cmass(ntot)

c      data Epoch,Epoch0/2451545.0d0,2000.0d0/
      Epoch=2451545.0d0
      Epoch0=2000.0d0
c
      open(9,file='.t',status='new',err=1010)
      write(9,*)'### this is a message from within ''radau'' ###'
      write(9,*)'    the integration has not started yet.'
      close(9)
 1010 open(unit=10,file=sysoutintegfile,status='new')
c
      gauss=0.295912208285591d-3
      zero=0.d0
c
c           nplan = number of planets
c           nobj  = number of asteroids
c           amass = mass of planet (solar masses)
c             x   = position vector   x,y,z    )
c                                              )  heliocen. equ. coord.
c             y   = velocity vector  x',y',z'  )
c
c           dlim  = limits for close encounters in  au.
c
c
c------------------------------------------------------------------
c           zmass = central mass
c           bmass =
c           cmass = masses in canonical units
c
c           axis  = semi major axis in au.
c
c
c     input from file: startval (lu 11)
c
      open(unit=11,file=startvalfile,status='old')
c! for transfer to analyse routine
      read (11,*) npla,nkast,ntiss
c  ! number of planets,objects  
      read (11,*) nplan,nobj      
c
      nbody=nplan+nobj
      if(nbody.gt.ntot) goto 580
c
c! startepoch (jd), centralmass (sun+(pl)
      read(11,*) start,zmass      
c
      do 10 k=1,nbody
      read(11,113,err=560,end=570) name(k),amass(k),dlim(k)
 113  format(5x,a20,d20.13,f10.2)
      read(11,102)(x(i),i=1,3),(y(i),i=1,3)
 102  format(3d22.14,/,3d22.14)
      r=zero
      v=zero
      j=(k-1)*3
      bmass(k)=amass(k)*gauss
      cmass(k)=dsqrt(zmass+bmass(k))
      do 5 i=1,3
      r=r+x(i)*x(i)
      v=v+y(i)*y(i)
      cor(j+i)=x(i)
   5  vel(j+i)=y(i)
      r=dsqrt(r)
      axis(k)=1.d0/(2.d0/r-v/(zmass+bmass(k)))
c
c     output to file sysout.lis (lu 10)
c
      write(10,302) name(k),dlim(k),axis(k)
 302  format(5x,a20,1x,f5.2,10x,f7.3)
c
   10 continue
c
      close(unit=11)
c
c     input from command file: radauint.com (lu 5)
c
      read(*,*) final,outint
c
c     start  = start epoch in jd
c     final  = end epoch in jd
c     outint = output interval in days
c
c     tint  = integration intervall - output at these epochs
c
      if (final.eq.start) goto 575

      time=start
      tint=outint
      dt=dabs(final-start)
      if (dt.lt.tint) tint=dt
      neq=nbody*3
c
c     sets sign for integration = backwards - forwards
c
      if(final.lt.start)tint=-tint
c
c
c     output on file approach (lu 13)
c
      open(unit=20,file=restartfile,status='unknown')
      close(unit=20)
      open(unit=13,file=approachfile,status='new',err=4201)
4201  open(unit=13,file=approachfile,status='old',
     #     err=4202)
4202  write(13,2230) (name(i),i=1,nplan)
2230  format(/' integration method:  radau'/' planets: ',9(a8,1x)//)
      write(13,223) start,final
 223  format(5x,'planetary encounters found at integr',
     *  'ation from',f10.1,' to',f10.1//3x,'object',4x,'planet',3x,
     *  'time(dm)',4x,'delta',5x,'time(bm)',4x,
     *  'bind. ene.',3x,'duration (yr)'/)
      close(unit=13)
c
c----------------------------------------------------------------------
c     definition of mode of integration in sr ra15
c
c
c     xl  sets first sequence size (in days)  if  ll  is positive
c
c          xl = 0  :  fss = 0.1 days
c
c     ll  negative sets constant sequence size  xl
c
c      xl = 5.d0
c      ll = 9
c
      read(*,*) xl,ll
      read(*,*) type_of_output
c
      open(unit=14,file=osculatefile,status='new',err=4203,
     &     form='unformatted')
4203  open(unit=14,file=osculatefile,status='old',
     &     err=4204,form='unformatted')
4204  write(14) npla,nkast,ntiss
      write(14) nplan,nobj
      close(unit=14)
c----------------------------------------------------------------------
c
c     store data at the beginning
c
      goto 42
c
c     loop over one output interval = tint
c
      
 40   call  ra15(cor,vel,tint,xl,ll,neq,-2,15)
c
c     control of data saving output interval
c
c
c     loop over all objects
c    
c     input data = output data stored in startval (lu 20)
c     at each output step
c
  42  open(unit=20,file=restartfile,status='unknown')
 1014 open(unit=14,file=osculatefile,status='old',access='append',
     &     form='unformatted')
c
      rewind 20
      write(20,*) npla,nkast,ntiss
      write(20,*) nplan,nobj
c,'     number of planets, objects'
      write(20,*) time,zmass
c,'     epoch (jd), central mass'
c
c ***    output stored in internal representation     ***
c  
      write(14) time
 101  format(a8)

      do j=1,nbody
         l=(j-1)*3
         do i=1,3
            x(i)=cor(l+i)
            y(i)=vel(l+i)
         end do
         if( type_of_output .eq. 0 )then
            if( j .gt. nplan )then
               write(14)name(j),(x(i),i=1,3)
            end if
         else
            write(14)name(j),(x(i),i=1,3),(y(i),i=1,3)
         endif
         write(20,113) name(j),amass(j),dlim(j)
         write(20,102)(x(i),i=1,3),(y(i),i=1,3)
      end do


      open(9,file='.t',status='unknown',err=1120)
      write(9,*)'### this is a message from within ''radau'' ###'
      write(9,'(a,f12.1)')'the integration is at time = ',time
      write(9,'(a,f12.1)')'final = ',final
      write(9,'(a,f5.1)')'% = ',100.0*(time-start)/(final-start)
      close(9)

 1120 close(unit=20)
 1114 close(unit=14)
c
c     check for end of integration
c
c
      if(dabs(time-start+tint).lt.dabs(final-start))go to 40
      tint=dabs(final-start)-dabs(time-start)
      if(final.lt.start) tint=-tint
      if(dabs(time-start).lt.dabs(final-start))goto 40
c
      goto 99
c
c     error messages
c
 560  write(10,561) 
 561  format(/' read error on lu 11')
      goto 99
c
 570  write(10,571)
 571  format(/' number of objects larger than expected')
      goto 99
c
 575  write(*,576)
      write(10,576)
 576  format(/' start epoch = final epoch')
      goto 99
c
 580  write(10,581)
 581  format(//' too many objects!'//)
  99  continue
      close(10)
      end






