c
c     *****************************************************************
c
c     program name:  initradau   version:  88-11-21
c
c     *****************************************************************
c
      implicit real*8 (a-h,o-z)

      include 'include.f'

c
      character*20 name
      character*5 nam2
c
      parameter (ncom=1000)
      parameter (npl=9)
      parameter (ntot=ncom+npl)
      parameter (ntot3=ntot*3)
c
      common /dist/ time,dlim(ntot),axis(ntot),ibody,name(ntot)
      common /body/ zmass,bmass(ntot),nplan
      common /const/ pi,grarad
      common /gauss/gauss
c
      dimension x(3),y(3),cor(ntot3),vel(ntot3),el(6),amass(ntot),
     *          cmass(ntot),inam(ncom)

c      character*6 inam(ncom), jnam
c
      dimension elem(7,ncom),nam2(5)
c      data name/ntot*'                    '/
      do i=1,ntot
            name(i)='                    '
      end do
c
      pi=atan(1.d0)*4.d0
      grarad=pi/1.8d2
      gauss=0.295912208285591d-3
c new value of eps
      eps=23.43929111d0*grarad
      ceps=dcos(eps)
      seps=dsin(eps)
      zero=0.d0

c      write(*,*)'here we go...'
      
      open(10,file=sysoutinit,status='unknown')
      open(17,file='names',status='unknown')
      open(11,file=plsysfile,status='old')
      open(15,file=elementfile,status='old')
 
c
c     input of planetary data  (lu 11)
c
c           nplan = number of planets
c           nkast = number of planets to be thrown into the sun
c           tplan = epoch for planets
c           amass = mass of planet (solar masses)
c             x   = position vector   x,y,z    )
c                                              )  heliocen. equ. coord.
c             y   = velocity vector  x',y',z'  )
c           nvel  = velocities in au/day or au/100days
c
c           dlim  = limits for close encounters in  au.
c
c
c------------------------------------------------------------------
c           zmass = central mass
c           bmass =
c           cmass = masses in canonical units
c
c           axis  = semi major axis in au.
c
c------------------------------------------------------------------
c
c     output to file sysout.lis (lu 10)
c
      write(10,301)
 301  format(//5x,'planets',10x,'encounter lim',10x,'sma'/)
c
c     input planetary data
c
c
c     input from command file
c
      read(*,*)nplan,nkast,ntiss
c
      read(11,*) tplan,nvel,nout
      dvel = nvel
      zmass = gauss
      zm=zero
      if(nkast.eq.0) goto 3
c
c     loop over planets to be thrown into the sun
c
      do 1 iplan=1,nkast
      read(11,113)name(iplan),amass(iplan),dlim(iplan)
      read(11,102)(x(i),i=1,3),(y(i),i=1,3)
 102  format(3d22.14,/,3d22.14)
  1   zm=zm + amass(iplan)
c
      zmass = gauss+zm*gauss
      nplan = nplan-nkast
c
c     loop over all planets to be used in the integration
c
  3   do 10 iplan=1,nplan
      read(11,113) name(iplan),amass(iplan),dlim(iplan)
      read(11,102)(x(i),i=1,3),(y(i),i=1,3)
      r=zero
      v=zero
      j=(iplan-1)*3
      bmass(iplan)=amass(iplan)*gauss
      cmass(iplan)=dsqrt(zmass+bmass(iplan))
c
      do 5 i=1,3
      r=r+x(i)*x(i)
      v=v+y(i)*y(i)/dvel/dvel
      cor(j+i)=x(i)
   5  vel(j+i)=y(i)/dvel
c
      r=dsqrt(r)
      axis(iplan)=1.d0/(2.d0/r-v/(zmass+bmass(iplan)))
c
c     output to file sysout.lis (lu 10)
c
      write(10,302) name(iplan),dlim(iplan),axis(iplan)
 302  format(5x,a20,1x,f5.2,10x,f7.3)
c
c     transformation to heliocent. ecl. coord.
c
      r=cor(j+2)*ceps+cor(j+3)*seps
      v=vel(j+2)*ceps+vel(j+3)*seps
      cor(j+3)=-cor(j+2)*seps+cor(j+3)*ceps
      vel(j+3)=-vel(j+2)*seps+vel(j+3)*ceps
      cor(j+2)=r
      vel(j+2)=v
   10 continue
c
      ibody=0
c
c     input data for object from data region (lu 5) bradaun.com
c
c           nobj  = number of objects
c           ibody = 0 = integration of planets only
c           nbody = number of objects = nplan + nobj
c           start = epoch of elements in jd
c
c           tint  = integration intervall from planetary epoch
c                   to epoch of osculation
c
c
      read(*,*) nobj
c
c----------------------------------------------------------------------
c     check for limiting number of objects allowed to be integrated
c
      if(nobj.gt.(ntot-nplan)) goto 580
c
c----------------------------------------------------------------------
c
      nbody=nplan+nobj
c
c     loop over all objects to be integrated
c
      do 100 i=1,nobj
      read(*,*) inam(i)
c      read(*,114) inam(i)
c 114  format(i4)
      if (inam(i).lt.1000) then
         write(name(nplan+i),'(i3.3)') inam(i)
      else if (inam(i).lt.10000) then
         write(name(nplan+i),'(i4.4)') inam(i)
      else if (inam(i).lt.100000) then
         write(name(nplan+i),'(i5.5)') inam(i)
      else
         write(name(nplan+i),'(i6.6)') inam(i)
      end if

c      write(*,*)'objecs read.'
c
c     input from element file (lu 15)
c
c     !!!!!! all elements have to have the same epoch !!!!
c
c
      rewind 15
c
c     input order of objects elements: a,e,i,node,peri,m,ep
c
      
      read(15,103,err=559) jnam
 103  format(i6,2x,2f10.7,4f10.5,f10.1,12x,5a5)
c itype = 0 asteroid
c         1 comet       
      itype=0
      goto 111
 559  itype=1
 111  rewind 15
 11   if (itype.eq.0) then
         read(15,103,err=560,end=577) jnam,(elem(l,i),l=1,7),
     *        nam2
      else
         read(15,1033,err=560,end=577) jnam,(elem(l,i),l=1,7),
     *        nam2
      end if
 1033 format(i6,2x,2f10.7,4f10.5,f10.1,13x,5a5)
      if( inam(i).eq.jnam) go to 12
      go to 11

c
 12   continue
      write (17,717) nam2
      write(10,103) jnam,(elem(l,i),l=1,7),nam2
      print *,'  Processing object ',inam(i),'  ',nam2
 717  format(5a5)
      if(i.eq.1) then
         write(10,104)
 104     format(//5x,' objects to be integrated:'/)
         ep1=elem(7,1)
      endif
      write(10,105) name(nplan+i)
 105  format(5x,a20)
      epc=elem(7,i)
      if(epc.ne.ep1) goto 565
c
 100  continue
c
      start=2400000.5d0+elem(7,1)
      if(elem(7,1).gt.100000.d0) start=elem(7,1)
      tint=start-tplan
c      write(*,*)'tint=',tint
      time=tplan
      neq=nplan*3
c
c     input from command file
c
c parameters for ra15, see below
      read(*,*) xl,ll
c
c     jump if planetary epoch the same as objects'
c
      if(dabs(tint).lt.1d-4) then
        write(10,*) '            planetary epoch the same as objects'
        goto 14
      endif
c
      write(10,220) start,tint,time
 220  format(/' start: jd ',f10.1,/' tint: ',f10.1,/' time:   jd',
     *f10.1)
      write(10,221)
 221  format(//5x,'integration of planetary system'//5x,'calls',2x,'sequ
     *ences',2x,'time span',3x,'stop at jd'/)
c
c
c     integration of planetary system
c
c
c     xl  sets first sequence size (in days)  if  ll  is positive
c
c          xl = 0  :  fss = 0.1 days
c
c     ll  negative sets constant sequence size  xl
c
c      xl = 5.d0 = first step size is 5 days
c      ll = 9
c
c
c      write(*,*)'going to ra15'
      call ra15(cor,vel,tint,xl,ll,neq,-2,15)
c      write(*,*)'returned from ra15'
c
c
  14  continue
c
c     output of planetary and objects data at startepoch 
c
c
c     output to file startval (lu 12)
c
c     ***   ecl. coord.   x,y,z  - x',y',z'   ***
c
c
c     transfer of objects' osculating elements to x,y,z and x',y',z'
c
      do 20 j=1,nobj
c
      do 21 jj=1,6
  21  el(jj)=elem(jj,j)
c
      ibody=nplan+j
c
c     masses of objects set to zero
c
      amass(ibody)=zero
      bmass(ibody)=zero
c
c     calculate  x,y,z, x',y',z' from osc. elements
c
c      write(*,*)'going to corvel'
      call corvel(el,x,y)
c      write(*,*)'returned from corvel'
c
c     velocities in canonical units form sr corvel
c
      cmass(ibody)=dsqrt(zmass)
      do 25 i=1,3
  25  y(i)=y(i)*cmass(ibody)
c
      ll=(ibody-1)*3
c
c     transfer of coordinates and velocities to vectors *cor* and *vel*
c     used by the integration routine
c
      do 30 i=1,3
      cor(ll+i)=x(i)
  30  vel(ll+i)=y(i)
c
  20  continue
c
c     output
c
      open(unit=12,file=startvalfile,status='unknown')
c      write(*,*)'startval is now open...'
c
      write(12,*) nplan+nkast,nkast,ntiss
c,'    nplan,nkast,ntiss'
      write(12,*) nplan,nobj
c,'    number of planets, objects'
      write(12,*) time,zmass
c,'   epoch in jd,   central mass'
c
      do 40 iobj=1,nbody
c
      j=(iobj-1)*3
c
      do 35 i=1,3
      x(i)=cor(j+i)
  35  y(i)=vel(j+i)
c
      write(12,113) name(iobj),amass(iobj),dlim(iobj)
 113  format(5x,a20,d20.13,f10.2)
  40  write(12,102)(x(i),i=1,3),(y(i),i=1,3)
c
      close(unit=12)
      close(11)
      close(17)
      close(10)
      goto 99
c
c     error messages
c
 560  write(10,561) 
      write(*,561) 
 561  format(/' read error on lu 15')
      goto 99
c
 565  write(10,566)
      write(*,566)
 566  format(/' epochs not equal')
      goto 99
c
 577  write(10,579)
      write(*,579)
 579  format(1x,'the asteroid was not found in the list')
      goto 99
c
 580  write(10,581)
      write(*,581)
 581  format(//' too many objects!'//)
  99  continue

      end

