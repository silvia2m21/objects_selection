c
c     *****************************************************************
c
c     program name:  analyse   version:  89-09-08, 89-12-05 (ml)
c
c     *****************************************************************
c
      implicit real*8 (a-h,o-z)

      include 'include.f'
c
c
      parameter (nast=1001)
      parameter (npl=10)
      parameter (ntot=nast+npl)
      parameter (ntot3=ntot*3)
      parameter(year0=2000.0d0,
     .          epoch0=2451545.0d0)
c     .          aj=5.2028)

      REAL*8 AP1s(9),AN1s(9),AI1s(9),AE1s(9),AA1s(9)
      REAL*8 AP1,AN1,AI1,AE1,AA1
      REAL*8 AP2,AN2,AI2,AE2,AA2
      real*8 node(3),inode(3),angmom(3),nodep(3),inodep(3),angmomp(3),
     #     cosi,auxil(3),prscal
      real*8 Ts(9),rmin,rmins(9),muti(9)

      character*3 number
      character*20 name(ntot)
      character*40 outfilename
c
      common /const/ pi,grarad
c
      dimension x(3),y(3),el(6), nrev(ntot),el6(ntot),timep(ntot)
      dimension amass(npl),bmass(npl),cmass(ntot)
c
      COMMON/ORB1/AP1,AN1,AI1,AE1,AA1
      COMMON/ORB2/AP2,AN2,AI2,AE2,AA2
c
      data nrev /ntot*0/
c
      pi=atan(1.d0)*4.d0
      grarad=pi/1.8d2
      gauss=0.295912208285591d-3
c input starting epoch
c
c    number of files to be processed
c
c      read(*,*) lastf
      lastf=1
c
c     control message to logfile after each ma:th block processed
c
      ma=100
c
      zero=0.d0
c
c---------------------------------------------------
c           zmass = central mass
c           amass = mass of planet (solar masses)
c           bmass = amass*gauss
c           cmass = masses in canonical units
c---------------------------------------------------
c
      open(11,file='plsys',status='old')
      read(11,*) dummy,idummy,idummy
      do  iplan=1,npl-1
         read(11,113,end=999) name(iplan),amass(iplan)
     #        ,dummy
         read(11,102)(x(i),i=1,3),(y(i),i=1,3)
      end do
 102  format(3d22.14,/,3d22.14)
 113  format(5x,a20,d20.13,f10.2)

 999  do 1 i=1,10
  1   bmass(i)=amass(i)*gauss
c
c
c---------------------------------------------------------------------------
c     input of data from command file (lu 5)
c
c
c           npla  = number of planets to be read in
c           nkast = number of planets to be added to the sun
c           nplan = number of planets to be used in the integration
c
c           nobj  = number of asteroids
c--------------------------------------------------------------------------
c
      luin=10
      nfile=1
 1000 open(unit=luin,file='osculate',status='old',form='unformatted')
      read(luin)npla,nkast,ntiss
      ntiss=ntiss-nkast
      read(luin)nplan,nobj
      if(npla.gt.iplan-1) then
        write(*,*) ' more planets than in plsys!'
        stop
      endif
      if((npla-nkast).ne.nplan) then
        write(*,*) ' number of planets in error!'
        stop
      endif
      write(*,*)' nplan,nobj read from file ',luin,' = ',nplan,nobj
      if(nfile.gt.1)goto 10
c
      zmass = gauss
      zm=zero
      if(nkast.eq.0) goto 3
c
c     loop over planets to be thrown into the sun
c
      do 2 i=1,nkast
  2   zm=zm + amass(i)
      zmass = gauss+zm*gauss
c
c     loop over all planets to be used in the integration
c
  3   nbody=nplan+nobj
      if(nbody.gt.ntot) goto 570 
c         !dimension limit exceeded!
c
      do 4 i=1,nplan
      j=i+nkast
  4   cmass(i)=dsqrt(zmass+bmass(j))
c
      do 5 i=nplan+1,nbody
  5   cmass(i)=dsqrt(zmass)
c
 10   continue
c   10 do 20 j=1,nplan
c      read(luin) nj,name(j)
c   20 write(*,250) nj,name(j)
c      do 30 j=1,nobj
c      read(luin) nj,name(j+nplan)
c   30 write(*,250) nj,name(j+nplan)
c  250 format(1x,i5,1x,a20)
c
c
c-------------------------------------------------------------
c     input from data file (lu luin)
c
c     data structure
c
c     time
c     x,y,z,x',y',z' one line for each object
c------------------------------------------------------------
c
c
c ***    input is stored in internal representation 'unformatted' ***
c
c
c----------------------------------------------------------------------
c            time = in julian days
c
c             x   = position vector   x,y,z
c
c             y   = velocity vector  x',y',z'
c----------------------------------------------------------------------
c
      nbl=0
  50  read(luin,err=500,end=600) time
      if(nfile.gt.1 .and.nbl.eq.0) then
        write(*,*) timeo,time
        if(timeo.ne.time) then
          write(*,*) ' discordant times!'
          stop
        endif
      endif
c
c     control message to logfile
c
      nbl=nbl+1     
c       ! counts number of data blocks
      if(mod(nbl,ma).eq.0)write(*,*) ' block ',nbl,' time ',time
c
      do 300 j=1,nbody
      read(luin,err=510) name(j),(x(i),i=1,3),(y(i),i=1,3)
      if(timeo.eq.time) goto 300
c
c     velocities in canonical units
c
      do 47 i=1,3
  47  y(i)=y(i)/cmass(j)
c
      call orbel(x,y,el,pq,gq,ran,rdn,tiss)
      if (j.eq.ntiss) aj=el(1)
c
c     calculates number of logical unit for output files, starting with 20
c     *** the files will be named  for020.dat, for021.dat, etc. ***
c
      luout=20+j-1
c
c     header on outputfile(s)
c
      write (number,'(i3.3)') luout
      outfilename='output'//number
c      call make_file_name(outputfile,luout,outfilename)
      open(luout,file=outfilename,status='new',err=4201)
4201  open(luout,file=outfilename,status='old',err=4202,access='append')
 4202 continue
      if(nbl.eq.1.and.j.le.nplan) write(luout,5001) name(j)
5001  format(//20x,a20//1x,'time (jd)',7x,'  a  ',8x,'  ecc. ',3x,
     *       '   incl   ',3x,' node',6x,' peri',6x,' m  ',5x,
     *       '  l. per.',6x,'q',7x,'Q',6x,'nrev'/)
      if(nbl.eq.1.and.j.gt.nplan) then 
         write(luout,5002) name(j)
         write(luout,5003) (name(jj),jj=1,nplan)
      endif
 5002 format(//20x,a20//)
 5003 format(1x,'time (jd)',7x,'  a  ',8x,'  ecc. ',3x,
     *       '   incl   ',3x,' node',6x,' peri',6x,' m  ',5x,
     *       '  l. per.',6x,'q',7x,'Q',6x,'nrev',
c     *     27('  Mut.Inc.    T  ',a8,'  MOID  ')/)
     *     18('   T  ',a8,'  MOID  ')/)
c
      if(nbl.gt.1) then
         if((timep(j).lt.time.and.el(6).lt.el6(j)).or.
     *        (timep(j).gt.time.and.el(6).gt.el6(j))) nrev(j)=nrev(j)+1
      endif
      el6(j)=el(6)
      timep(j)=time
      ql=el(4)+el(5)
      if(ql.gt.360.d0) ql=ql-360.d0
      year=(time-epoch0)/365.25+year0
c      year=time/365.25

      if (j.le.nplan.or.el(1).lt.0.) then
         if (abs(year).lt.1.e6) then 
            if (el(1).lt.1.e3.and.gq.lt.1.e3) then 
               write(luout,200)year,(el(i),i=1,6),ql,pq,gq,nrev(j)
            else 
               write(luout,202)year,(el(i),i=1,6),ql,pq,gq,nrev(j)
            end if
         else 
            if (el(1).lt.1.e3.and.gq.lt.1.e3) then 
               write(luout,201)year,(el(i),i=1,6),ql,pq,gq,nrev(j)
            else
               write(luout,203)year,(el(i),i=1,6),ql,pq,gq,nrev(j)
            end if
         end if
         AA1s(j)=el(1)
         AE1s(j)=el(2)
         AI1s(j)=el(3)
         AN1s(j)=el(4)
         AP1s(j)=el(5)

      else
         AA2=el(1)
         AE2=el(2)
         AI2=el(3)
         AN2=el(4)
         AP2=el(5)
         node(1)=cos(AN2*grarad)
         node(2)=sin(AN2*grarad)
         node(3)=0
         inode(1)=-sin(AN2*grarad)*cos(AI2*grarad)
         inode(2)=cos(AN2*grarad)*cos(AI2*grarad)
         inode(3)=sin(AI2*grarad)
         call prvec(node,inode,auxil)
         call normal(auxil,angmom)
c         if (AI2.gt.90.) angmom(3)=-angmom(3)

         do i=1,nplan
            AA1=AA1s(i)
            AE1=AE1s(i)
            AI1=AI1s(i)
            AN1=AN1s(i)
            AP1=AP1s(i)
            call moid(rmin)
            rmins(i)=rmin
            nodep(1)=cos(AN1*grarad)
            nodep(2)=sin(AN1*grarad)
            nodep(3)=0
            inodep(1)=-sin(AN1*grarad)*cos(AI1*grarad)
            inodep(2)=cos(AN1*grarad)*cos(AI1*grarad)
            inodep(3)=sin(AI1*grarad)
            call prvec(nodep,inodep,auxil)
            call normal(auxil,angmomp)
c            if (AI1.gt.90.) angmomp(3)=-angmomp(3)
            cosi=prscal(angmom,angmomp)
            Ts(i)=AA1/AA2+2.d0*sqrt(AA2/AA1*(1.d0-AE2*AE2))*cosi
            muti(i)=acos(cosi)/grarad
         enddo
         if (abs(year).lt.1.e6) then 
            if (el(1).lt.1.e3.and.gq.lt.1.e3) then 
               write(luout,200)year,(el(i),i=1,6),ql,pq,gq,nrev(j)
     #              ,(Ts(i),rmins(i),i=1,nplan)
            else 
               write(luout,202)year,(el(i),i=1,6),ql,pq,gq,nrev(j)
     #              ,(Ts(i),rmins(i),i=1,nplan)
            end if
         else 
            if (el(1).lt.1.e3.and.gq.lt.1.e3) then 
               write(luout,201)year,(el(i),i=1,6),ql,pq,gq,nrev(j)
     #              ,(Ts(i),rmins(i),i=1,nplan)
            else
               write(luout,203)year,(el(i),i=1,6),ql,pq,gq,nrev(j)
     #              ,(Ts(i),rmins(i),i=1,nplan)
            end if
         end if
c         if (abs(year).lt.1.e6) then 
c            if (gq.lt.1.e3) then 
c               write(luout,200)year,(el(i),i=1,6),ql,pq,gq,nrev(j)
c     #              ,(muti(i),Ts(i),rmins(i),i=1,nplan)
c            else 
c               write(luout,202)year,(el(i),i=1,6),ql,pq,gq,nrev(j)
c     #              ,(muti(i),Ts(i),rmins(i),i=1,nplan)
c            end if
c         else 
c            if (gq.lt.1.e3) then 
c               write(luout,201)year,(el(i),i=1,6),ql,pq,gq,nrev(j)
c     #              ,(muti(i),Ts(i),rmins(i),i=1,nplan)
c            else
c               write(luout,203)year,(el(i),i=1,6),ql,pq,gq,nrev(j)
c     #              ,(muti(i),Ts(i),rmins(i),i=1,nplan)
c            end if
c         end if

      end if
      close(unit=luout)
 200  format(1x,f12.4,2(1x,f11.5),5(1x,f10.4),1x,f9.5,f9.4,i6
     #,27(1x,f10.5))
 202  format(1x,f12.4,1x,f11.0,1x,f11.5,5(1x,f10.4),1x,f9.5,f9.0
     #,i6,27(1x,f10.5))
 201  format(1x,f12.0,2(1x,f11.5),5(1x,f10.4),1x,f9.5,f9.4,i6
     #,27(1x,f10.5))
 203  format(1x,f12.0,1x,f11.0,1x,f11.5,5(1x,f10.4),1x,f9.5,f9.0
     #,i6,27(1x,f10.5))
 300  continue
c
      goto 50
c
c     error messages
c
 500  write(*,*) ' read error at block ',nbl+1
      goto 99
c
 510  write(*,*) ' read error at block ',nbl,' object no ',i+1
      goto 99
c
 570  write(*,571)
 571  format(/' number of objects larger than expected')
      goto 99
c
 600  write(*,*) nbl,' data blocks processed from file ',nfile
      timeo=time
      nfile=nfile+1
      close(luin)
      if(nfile.gt.lastf) goto 99
      goto 1000
  99  stop
      end
c
c------------------------------------------------------------------
c
      subroutine orbel(x,v,elem,pq,gq,ran,rdn,tiss)
c
      implicit real*8 (a-h,o-z)
c
      common /const/ pi,grarad
c
      dimension el(6),x(3),v(3),elem(6)
c
      aj=5.202803d0
      c1=x(2)*v(3)-x(3)*v(2)
      c2=x(3)*v(1)-x(1)*v(3)
      c3=x(1)*v(2)-x(2)*v(1)
      rv=x(1)*v(1)+x(2)*v(2)+x(3)*v(3)
      acca2=c1*c1+c2*c2+c3*c3
      acca=dsqrt(acca2)
      el(5)=datan2(dsqrt(c1*c1+c2*c2),c3)
      cosi=dcos(el(5))
      sini=dsqrt(1.d0-cosi*cosi)
      el(6)=datan2(c1,-c2)
      cosog=dcos(el(6))
      sinog=dsin(el(6))
      el(5)=el(5)/grarad
      el(6)=el(6)/grarad
      if(el(6).lt.0.d0) el(6)=el(6)+360.d0
      r0=dsqrt(x(1)*x(1)+x(2)*x(2)+x(3)*x(3))
      u0=datan2((-x(1)*sinog+x(2)*cosog)*cosi+x(3)*sini,x(1)*cosog+x(2)
     1 *sinog)
      f0=datan2(acca*rv,acca2-r0)
      el(4)=(u0-f0)/grarad
      if(el(4).lt.0.d0) el(4)=el(4)+360.d0
      if(el(4).gt.3.6d2) el(4)=el(4)-360.d0
      v02=v(1)*v(1)+v(2)*v(2)+v(3)*v(3)
      z=2.d0/r0-v02
      el(2)=-1.d20
      if(z.ne.0.d0) el(2)=1.d0/z
      ex2=1.d0-acca2*z
      if(ex2.ge.0.d0) go to 1
      write(*,222)
  222 format(//10x,'error in angular momentum')
      el(1)=0.d0
      go to 2
    1 el(1)=dsqrt(ex2)
      pq=acca2/(1.d0+el(1))
      tiss=aj*z+2.d0*acca/dsqrt(aj)*cosi
      if(el(2)) 2,2,3
    2 gq=9999999.
      el(3)=999.999
      goto 900
    3 cose=(1.d0-r0/el(2))/el(1)
      if(cose.le.1.d0) go to 4
      write(*,333)
  333 format(//10x,'error in eccentric anomaly')
      goto 900
    4 sine=dsqrt(1.d0-cose*cose)
      if(rv.lt.0.d0) sine=-sine
      e=datan2(sine,cose)
      el(3)=(e-el(1)*dsin(e))/grarad
      if(el(3).lt.0.d0) el(3)=el(3)+360.d0
      gq=2.d0*el(2)-pq
      vn=(360.d0-el(4))*grarad
      p=el(2)*(1.d0-el(1)*el(1))
      ran=p/(1.d0+el(1)*dcos(vn))
      rdn=p/(1.d0-el(1)*dcos(vn))
c
c     reordering of elements from e,a,m,peri,i,node ==> a,e,i,node,peri,m
c
 900  continue
      elem(1)=el(2)
      elem(2)=el(1)
      elem(3)=el(5)
      elem(4)=el(6)
      elem(5)=el(4)
      elem(6)=el(3)
      return
      end

