c
c------------------------------------------------------------------
c
      subroutine ra15(x,v,tf,xl,ll,nv,nclass,nor)
c
c
c  integrator radau by e. everhart, physics department, university of denver
c
c
c  this 15th-order version is called ra15.
c  y'=f(y,t) is nclass=1, y"=f(y,t) is nclass=-2, y"=f(y',y,t) is nclass
c  tf is t(final) - t(initial).it may be negative for backward integration
c  nv = the number of simultaneous differential equations.
c  change if necessary for a larger number of equations.
c  ll controls sequence size.thus ss=10**(-ll) controls the size of a te
c  a typical ll-value is in the range 6 to 12 for this order 15 program.
c  however, if ll.lt.o then xl is the constant sequence size used.
c  a non-zero xl sets the size of the first sequence regardless of ll's
c  x and v enter as the starting position-velocity vector, and are output
c  the final position-velocity vector.
c  integration is in double precision.a 64-bit double-word is assumed.
c
      implicit real*8 (a-h,o-z)
      real *4 pw,tval
      character*20 name

      include 'include.f'
c
      parameter (nast=1000)
      parameter (npl=9)
      parameter (ntot=nast+npl)
      parameter (ntot3=ntot*3)
c
      common /dist/ time,dlim(ntot),axis(ntot),ibody,name(ntot)
c
      dimension x(ntot3),v(ntot3),f1(ntot3),fj(ntot3),c(21),d(21),
     *          r(21),y(ntot3),z(ntot3),b(7,ntot3),g(7,ntot3),
     *          e(7,ntot3),bd(7,ntot3),h(8),w(7),u(7),nw(8)
c
      logical npq,nsf,nper,ncl,nes
      data nw/0,0,1,3,6,10,15,21/
      data zero, half, one,sr/0.0d0, 0.5d0, 1.0d0,1.4d0/
c
c  these h values are the gauss-radau spacings, scaled to the range 0 to 1
c  for integrating to order 15.    h(1) = 0.d0 always.
c
      data h/         0.d0,.0562625605369221d0,.1802406917368924d0,
     a.3526247171131696d0,.5471536263305554d0,.7342101772154105d0,
     b.8853209468390958d0, .9775206135612875d0/
c
c  the sum of these h-values should be 3.733333333333333
c
      nper=.false.
      nsf=.false.
      ncl=nclass.eq.1
      npq=nclass.lt.2
c
c  y'=f(y,t)  ncl=.true.    y"=f(y,t)  ncl=.false.   y"=f(y',y,t) ncl=.false
c  nclass=1   npq=.true.    nclass= -2 npq=.true.    nclass= 2    npq=.false
c  nsf is .false. on starting sequence, otherwise .true.
c  nper is .true. only on last sequence of the integration.
c  nes is .true. only if ll is negative.then the sequence size is xl.
c
      dir=one
      if(tf.lt.zero) dir=-one
      nes=ll.lt.0
      xl=dabs(xl)*dir
      pw=1.d0/9.d0
c
c  evaluate the constants in the w-, u-, c-, d-, and r-vectors
c
      do 14 n=2,8
      ww=n+n*n
      if(ncl) ww=n
      w(n-1)=one/ww
      ww=n
  14  u(n-1)=one/ww
      do 22 k=1,nv
      if(ncl) v(k)=zero
      do 22 l=1,7
      bd(l,k)=zero
  22  b(l,k)=zero
c
      w1=half
      if(ncl) w1=one
      c(1)=-h(2)
      d(1)=h(2)
      r(1)=one/(h(3)-h(2))
      la=1
      lc=1
      do 73 k=3,7
      lb=la
      la=lc+1
      lc=nw(k+1)
      c(la)=-h(k)*c(lb)
      c(lc)=c(la-1)-h(k)
      d(la)=h(2)*d(lb)
      d(lc)=-c(lc)
      r(la)=one/(h(k+1)-h(2))
      r(lc)=one/(h(k+1)-h(k))
      if(k.eq.3) go to 73
      do 72 l=4,k
      ld=la+l-3
      le=lb+l-4
      c(ld)=c(le)-h(k)*c(le+1)
      d(ld)=d(le)+h(l-1)*d(le+1)
   72 r(ld)=one/(h(k+1)-h(l-1))
   73 continue
      ss=10.d0**(-ll)
c
c  the statements above are used only once in an integration to set up the
c  constants. they use less than a second of execution time. next set in
c  a reasonable estimate to tp based on experience. same design as dir.
c  an initial first sequence size can be set with xl even with ll positiv.
c
      tp=0.1d0*dir
      if(xl.ne.zero)  tp=xl
      if(nes)  tp=xl
      if(tp/tf.gt.half) tp=half*tf
c! counts number of restarts
      ncount=0     
c
c  an * is the symbol for writing on the monitor.the printer is unit 4.
c  line 4000 is the starting place of the first sequence.
c
c! counts number of sequences
 4000 ns=0      
c! counts number of call to force routine
      nf=0      
      ni=6
      tm=zero
      call force (x, v, zero, f1)
      nf=nf+1
c
c  line 722 is begins every sequence after the first.first find new beta
c  values from the predicted b-values, following eq. (2.7) in text.
c
 722  do 58 k=1,nv
      g(1,k)=b(1,k)+d(1)*b(2,k)+
     x  d(2)*b(3,k)+d(4)*b(4,k)+d( 7)*b(5,k)+d(11)*b(6,k)+d(16)*b(7,k)
      g(2,k)=         b(2,k)+
     x  d(3)*b(3,k)+d(5)*b(4,k)+d( 8)*b(5,k)+d(12)*b(6,k)+d(17)*b(7,k)
      g(3,k)=b(3,k)+d(6)*b(4,k)+d( 9)*b(5,k)+d(13)*b(6,k)+d(18)*b(7,k)
      g(4,k)=            b(4,k)+d(10)*b(5,k)+d(14)*b(6,k)+d(19)*b(7,k)
      g(5,k)=                         b(5,k)+d(15)*b(6,k)+d(20)*b(7,k)
      g(6,k)=                                      b(6,k)+d(21)*b(7,k)
  58  g(7,k)=                                                   b(7,k)
      t=tp
      t2=t*t
      if(ncl) t2=t
      tval=dabs(t)
c     jump if only planets
      if(ibody.eq.0)go to 63   
c
c     check for close encounters
c
      nobf=nplan+1
      do 100 nobj=nobf,ibody
      j=nobj*3
      rr=dsqrt(x(j-2)*x(j-2)+x(j-1)*x(j-1)+x(j)*x(j))
      do 60 iplan=1,nplan
      test=dabs(rr-axis(iplan))
      if(test.gt.1.5d0*dlim(iplan)) go to 60
      jp=iplan*3
      dd=dsqrt((x(j-2)-x(jp-2))*(x(j-2)-x(jp-2))+(x(j-1)-x(jp-1))*
     *        (x(j-1)-x(jp-1))+(x(j)-x(jp))*(x(j)-x(jp)))
      if(dd.gt.dlim(iplan)) go to 60
      rp=zero
      vv=zero
      tid=time+tm
      do 59 i=1,3
      vv=vv+(v(j-3+i)-v(jp-3+i))*(v(j-3+i)-v(jp-3+i))
   59 rp=rp+x(jp-3+i)*x(jp-3+i)
      rp=dsqrt(rp)
      bind=gauss/half/dd-vv
      open(13,file=ApproachFile,status='new',err=1013)
1013  open(unit=13,file=ApproachFile,status='old',
     &     err=1014)
1014  write(13,7)name(iplan),rp,rr,dd,bind,tid,name(nobj)
    7 format(1x,a8,2x,3f10.4,5x,d10.3,5x,f15.5,5x,a20)
 1113 close(unit=13)
   60 continue
  100 continue
c
c  loop 175 is 6 iterations on first sequence and two iterations thereafter
c
   63 do 175 m=1,ni
c
c  loop 174 is for each substep within a sequence.
c
      if(.not.nsf.and.ni.ne.6) write (10,98) ni
      if(nsf.and.ni.ne.2) write (10,99) ni
98    format (20x,'ni = ',i3,' first sequence')
99    format (20x,'ni = ',i3)
c
      do 174 j=2,8
      jd=j-1
      jdm=j-2
      s=h(j)
      q=s
      if(ncl) q=one
c
c  use eqs. (2.9) and (2.10) of text to predict  positions at each substep
c  these collapsed series are broken into two parts because c 2060
c  excellent compiler could not handle the complicated expression
c
      do 130 k=1,nv
      a=w(3)*b(3,k)+s*(w(4)*b(4,k)+s*(w(5)*b(5,k)+s*(w(6)*b(6,k)+
     v   s*w(7)*b(7,k))))
      y(k)=x(k)+q*(t*v(k)+t2*s*(f1(k)*w1+s*(w(1)*b(1,k)+s*(w(2)*b(2,k)
     x  +s*a))))
      if(npq) go to 130
c
c  next are calculated the velocity predictors need for general class ii
c
      a=u(3)*b(3,k)+s*(u(4)*b(4,k)+s*(u(5)*b(5,k)+s*(u(6)*b(6,k)+
     t    s*u(7)*b(7,k))))
      z(k)=v(k)+s*t*(f1(k)+s*(u(1)*b(1,k)+s*(u(2)*b(2,k)+s*a)))
 130  continue
c
c  find forces at each substep
c
      call force(y,z,tm+s*t,fj)
      nf=nf+1
      do 171 k=1,nv
c
c  find g-value for the force fj found at the current substep. this
c  section, including the many-goto uses eq. (2.4) of text.
c
      temp=g(jd,k)
      gk=(fj(k)-f1(k))/s
      go to (102,102,103,104,105,106,107,108),j
 102  g(1,k)=gk
      go to 160
 103  g(2,k)=(gk-g(1,k))*r(1)
      go to 160
 104  g(3,k)=((gk-g(1,k))*r(2)-g(2,k))*r(3)
      go to 160
 105  g(4,k)=(((gk-g(1,k))*r(4)-g(2,k))*r(5)-g(3,k))*r(6)
      go to 160
 106  g(5,k)=((((gk-g(1,k))*r(7)-g(2,k))*r(8)-g(3,k))*r(9)-
     x     g(4,k))*r(10)
      go to 160
 107  g(6,k)=(((((gk-g(1,k))*r(11)-g(2,k))*r(12)-g(3,k))*r(13)-
     x     g(4,k))*r(14)-g(5,k))*r(15)
      go to 160
 108  g(7,k)=((((((gk-g(1,k))*r(16)-g(2,k))*r(17)-g(3,k))*r(18)-
     x     g(4,k))*r(19)-g(5,k))*r(20)-g(6,k))*r(21)
c
c  upgrade all b-values.
c
 160  temp=g(jd,k)-temp
      b(jd,k)=b(jd,k)+temp
c
c  temp is now the improvement on g(jd,k) over its former value.
c  now we upgrade the b-value using this difference in the one term.
c  this section is based on eq. (2.5).
c
      go to (171,171,203,204,205,206,207,208),j
 203  b(1,k)=b(1,k)+c(1)*temp
      go to 171
 204  b(1,k)=b(1,k)+c(2)*temp
      b(2,k)=b(2,k)+c(3)*temp
      go to 171
 205  b(1,k)=b(1,k)+c(4)*temp
      b(2,k)=b(2,k)+c(5)*temp
      b(3,k)=b(3,k)+c(6)*temp
      go to 171
 206  b(1,k)=b(1,k)+c(7)*temp
      b(2,k)=b(2,k)+c(8)*temp
      b(3,k)=b(3,k)+c(9)*temp
      b(4,k)=b(4,k)+c(10)*temp
      go to 171
 207  b(1,k)=b(1,k)+c(11)*temp
      b(2,k)=b(2,k)+c(12)*temp
      b(3,k)=b(3,k)+c(13)*temp
      b(4,k)=b(4,k)+c(14)*temp
      b(5,k)=b(5,k)+c(15)*temp
      go to 171
 208  b(1,k)=b(1,k)+c(16)*temp
      b(2,k)=b(2,k)+c(17)*temp
      b(3,k)=b(3,k)+c(18)*temp
      b(4,k)=b(4,k)+c(19)*temp
      b(5,k)=b(5,k)+c(20)*temp
      b(6,k)=b(6,k)+c(21)*temp
 171  continue
 174  continue
      if(nes.or.m.lt.ni) go to 175
c
c  integration of sequence is over.next is sequence size control.
c
      hv=zero
      do 635 k=1,nv
 635  hv=dmax1(hv,dabs(b(7,k)))
      hv=hv*w(7)/tval**7
 175  continue
      if (nsf) go to 180
      if(.not.nes) tp=(ss/hv)**pw*dir
      if(nes) tp=xl
      if(nes) go to 170
      if(tp/t.gt.one) go to 170
      tp=.8d0*tp
      ncount=ncount+1
c
      write(10,8) nor,ncount,t,tp,time
   8  format (/5x,'restart with 0.8 time seq.size: ',2i2,2f10.5,
     *f15.1/)
      if(ncount.gt.10) then
        write(10,*) 'more than 10 restarts'
        return
      endif
c
c  restart with 0.8x sequence size if new size called for is smaller than
c  originally chosen starting sequnce size on first sequence.
c
      go to 4000
 170  nsf=.true.
c
c  loop 35 finds new x and v values at end of sequence using eqs. (2.11)
c
 180  do 35 k=1,nv
      x(k)=x(k)+v(k)*t+t2*(f1(k)*w1+b(1,k)*w(1)+b(2,k)*w(2)+b(3,k)*w(3)
     x    +b(4,k)*w(4)+b(5,k)*w(5)+b(6,k)*w(6)+b(7,k)*w(7))
      if (ncl) go to 35
      v(k)=v(k)+t*(f1(k)+b(1,k)*u(1)+b(2,k)*u(2)+b(3,k)*u(3)
     v    +b(4,k)*u(4)+b(5,k)*u(5)+b(6,k)*u(6)+b(7,k)*u(7))
  35  continue
      tm=tm+t
      ns=ns+1
c
c  return if done.
c
      if(.not.nper) go to 78
      time=time+tm
c      write(10,*) ns,t,tp,dt,tm
      if (.not.nes) xl=tpo
      return
c
c  control on size of next sequence and adjust last sequence to exactly
c  cover the integration span. nper=.true. set on last sequence.
c
 78   call force (x,v,tm,f1)
      nf=nf+1
      if(nes) goto 341
      tp=dir*(ss/hv)**pw
      if(tp/t.gt.sr) tp=t*sr
  341 if (nes) tp=xl
      dt=tp-t
c      write(10,*) ns,t,tp,dt,tm
      if (dir*(tm+tp).lt.dir*tf-1.d-8) go to 77
      tpo=tp
      tp=tf-tm
      nper=.true.
c
c  now predict b-values for next step. the predicted values from the prev.
c  sequence were saved in the e-matrix.the correction bd between the actual
c  b-values found and these predicted values is applied in advance to the
c  next sequence.the gain in accuracy is significant.using eqs. (2.13):
c
  77  q=tp/t
      do 39 k=1,nv
      if(ns.eq.1) go to 31
      do 20 j=1,7
  20  bd(j,k)=b(j,k)-e(j,k)
  31  e(1,k)=      q*(b(1,k)+ 2.d0*b(2,k)+ 3.d0*b(3,k)+
     x           4.d0*b(4,k)+ 5.d0*b(5,k)+ 6.d0*b(6,k)+ 7.d0*b(7,k))
      e(2,k)=                q**2*(b(2,k)+ 3.d0*b(3,k)+
     y           6.d0*b(4,k)+10.d0*b(5,k)+15.d0*b(6,k)+21.d0*b(7,k))
      e(3,k)=                             q**3*(b(3,k)+
     z           4.d0*b(4,k)+10.d0*b(5,k)+20.d0*b(6,k)+35.d0*b(7,k))
      e(4,k)=   q**4*(b(4,k)+ 5.d0*b(5,k)+15.d0*b(6,k)+35.d0*b(7,k))
      e(5,k)=                q**5*(b(5,k)+ 6.d0*b(6,k)+21.d0*b(7,k))
      e(6,k)=                             q**6*(b(6,k)+ 7.d0*b(7,k))
      e(7,k)=                                           q**7*b(7,k)
      do 39 l=1,7
  39  b(l,k)=e(l,k)+bd(l,k)
c
c  two iterations for every sequence after the first.
c
      ni=2
      go to 722
      end
c
c------------------------------------------------------------------
c
      subroutine force(cor,vel,time,acc)
      implicit real*8 (a-h,o-z)
c
      include 'include.f'

      parameter (nast=1000)
      parameter (npl=9)
      parameter (ntot=nast+npl)
      parameter (ntot3=ntot*3)
c
      common/body/ gauss,amass(ntot),nbody
      dimension cor(ntot3),vel(ntot3),acc(ntot3),fmat(3,ntot),x(3)
      do 100 i=1,nbody
      j= (i-1)*3
      do 50 k=1,3
  50  x(k) =cor(j+k)
      r=dsqrt(x(1)*x(1)+x(2)*x(2)+x(3)*x(3))
      do 100 k=1,3
      fmat(k,i)= x(k)/r/r/r
      acc(j+k) =-(gauss+amass(i))*fmat(k,i)
  100 continue
      n1=nbody-1
      do 200 i=1,n1
      j=(i-1)*3
      i1=i+1
      do 200 l=i1,nbody
      m=(l-1)*3
      do 150 k=1,3
  150 x(k)=cor(j+k)-cor(m+k)
      r=dsqrt(x(1)*x(1)+x(2)*x(2)+x(3)*x(3))
      do 200 k=1,3
      p=x(k)/r/r/r
      acc(j+k)=acc(j+k)-amass(l)*(p+fmat(k,l))
      acc(m+k)=acc(m+k)-amass(i)*(-p+fmat(k,i))
  200 continue
      return
      end
c
c------------------------------------------------------------------
c
      subroutine corvel(el,x,v)
c
      common /const/ pi,grarad
c
      include 'include.f'

      double precision pi,grarad,e,a,m,i,gom,pom,k1,k2,k3,k4,k5,k6
     * ,el(6),p(3),q(3),x(3),v(3)
c
c! eccentricity
      e=el(2)                         
      if(e.ge.1.d0) go to 900
c! semimajor axis
      a=el(1)                         
c! mean anomaly
      m=el(6)*grarad                  
c! arg. of perihelion
      pom=el(5)*grarad                
c! inclination
      i=el(3)*grarad                  
c! long. of asc. node
      gom=el(4)*grarad                
      k1=dcos(pom)
      k2=dsin(pom)
c
c          coeffs cas plan
c
      if(i.gt.1.d-14) go to 200
      p(1)=k1
      q(1)=-k2
      p(2)=k2
      q(2)=k1
      go to 300
c
c          coeffs cas 3-d
c
  200 k3=dcos(i)
      k4=dcos(gom)
      k5=dsin(gom)
      k6=k3*k5
      p(1)=k1*k4-k2*k6
      q(1)=-k2*k4-k1*k6
      k6=k3*k4
      p(2)=k1*k5+k2*k6
      q(2)=-k2*k5+k1*k6
      k6=dsin(i)
      p(3)=k2*k6
      q(3)=k1*k6
c
  300 k1=dsqrt(1.d0-e*e)*a
      do 400 j=1,3
      p(j)=p(j)*a
      q(j)=q(j)*k1
      if(i.le.1.d-14.and.j.eq.2) go to 500
  400 continue
c
c          anom. exc.
c
  500 k1=m
  600 k2=k1
      k1=m+e*dsin(k2)
      if(dabs(k1-k2).gt.1.d-13) go to 600
      k3=dcos(k1)
      k4=dsin(k1)
      k5=k3-e
      k6=dsqrt(1.d0/a)/((1.d0-e*k3)*a)
c
      do 700 j=1,3
      x(j)=0.d0
      v(j)=0.d0
      if(j.gt.2.and.i.le.1.d-14) go to 700
      x(j)=p(j)*k5+q(j)*k4
      v(j)=(-p(j)*k4+q(j)*k3)*k6
  700 continue
      return
  900 write(10,9000) e
 9000 format(//10x,23horbite hyperbolique..e=,e13.5)
      return
      end


