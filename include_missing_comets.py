import os
import re

def include_missing_comets(missing,elem_mpcorb):

    # With file missing get the elements of missing comets in JPL

    if os.path.isfile('elem_missing'):
        os.remove('elem_missing')

    with open('elem_missing', 'a') as f1:
        with open(missing, 'r') as f2:
            for l1 in f2.readlines():
                l1 = re.sub(' +', ' ', l1)
                parts = l1.split(' ')
                i = parts[0]
                n = parts[1]
                name = parts[2]
                with open(elem_mpcorb) as f3:
                    aux = 0
                    for l2 in f3.readlines():
                        l2 = re.sub(' +', ' ', l2)
                        parts2 = l2.split(' ')
                        if aux < 100000:
                            if parts2[1] == i:
                                p0 = "%6d" % float(n)
                                p1 = "%10.7f" % float(parts2[2])
                                p2 = "%10.7f" % float(parts2[3])
                                p3 = "%10.5f" % float(parts2[4])
                                p4 = "%10.5f" % float(parts2[5])
                                p5 = "%10.5f" % float(parts2[6])
                                p6 = "%10.5f" % float(parts2[7])
                                p7 = "%10s" % parts2[8]
                                p8 = "%s" % name
                        else:
                            if parts2[0] == i:
                                p0 = "%6d" % float(n)
                                p1 = "%10.7f" % float(parts2[1])
                                p2 = "%10.7f" % float(parts2[2])
                                p3 = "%10.5f" % float(parts2[3])
                                p4 = "%10.5f" % float(parts2[4])
                                p5 = "%10.5f" % float(parts2[5])
                                p6 = "%10.5f" % float(parts2[6])
                                p7 = "%10s" % parts2[7]
                                p8 = "%s" % name
                        aux = aux + 1
                    f1.write(p0 + '  ' + p1 + '' + p2 + '' + p3 + '' + p4 + '' + p5 + '' + p6 + '' + p7 + '         ' + p0
                             + ' ' + p8 )


def create_files():

    # Create file elem_jpl with the elements of all comets
    # Create file number with the number of all comets

    if os.path.isfile('auxil'):
        os.remove('auxil')
    if os.path.isfile('number'):
        os.remove('number')

    with open('elem_jpl', 'a+') as f1:
        with open('elem_missing') as f2:
            for l in f2:
                f1.write(l)
    os.system('sort -n elem_jpl > auxil')
    os.system('mv auxil elem_jpl')

    with open('number','a') as f3:
        with open('elem_jpl','r') as f4:
            for ll in f4:
                ll = re.sub(' +', ' ', ll)
                ps = ll.split(' ')
                f3.write(ps[1] + '\n')