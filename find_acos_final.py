import os
import re
import shutil


def find_names(current_date):

    # Remove acos with no encounters
    # Create files with elements for resulting acos separated by type

    orbits = ['sel','remove']

    for i in orbits:
        if os.path.exists('number_names_jf_' + i):
            os.remove('number_names_jf_' + i)

        with open('acos_jf_' + i) as f:
            with open('number_names_jf_' + i,'a') as ff:
                for l in f:

                    with open('number_names') as fff:
                        for ll in fff:
                            sb = re.sub(' +', ' ', ll)
                            sp = sb.split(' ')
                            if sp[0] == '':
                                if l.replace('\n','') == sp[1]:
                                    ff.write(ll)
                            else:
                                if l.replace('\n','') == sp[0]:
                                    ff.write(ll)

    shutil.copyfile('number_names_jf_sel','number_names_final')

    with open('number_names') as f:
        with open('number_names_final','a') as ff:

            for l in f:
                sb = re.sub(' +', ' ', l)
                sp = sb.split(' ')

                if sp[0] == '':
                    if float(sp[4]) > 1:
                        ff.write(l)
                else:
                    if float(sp[3]) > 1:
                        ff.write(l)

    files = ['number_final','names_final','nameslong_final']

    for i in range(len(files)):
        if os.path.exists(files[i]):
            os.remove(files[i])

    with open('number_names_final') as f:
        with open('number_final','a') as f2:
            with open('names_final','a') as f3:
                with open('nameslong_final','a') as f4:

                    for l in f:
                        sb = re.sub(' +', ' ', l)
                        sp = sb.split(' ')

                        if sp[0] =='':
                            f2.write(sp[1] + '\n')
                            f3.write(sp[3] + '\n')
                            f4.write(sp[2] + '\n')
                        else:
                            f2.write(sp[0] + '\n')
                            f3.write(sp[2] + '\n')
                            f4.write(sp[1] + '\n')

    os.system('ln -s ../../elements/' + current_date + '/elem_acos_aeiTMOID')


    files_elem = ['elem_acos_jf','elem_acos_cen','elem_acos_aho']
    files_aei = ['elem_acos_jf_aeiTMOID','elem_acos_cen_aeiTMOID','elem_acos_aho_aeiTMOID']

    for j in range(len(files_elem)):

        if os.path.exists(files_elem[j]):
            os.remove(files_elem[j])

        if os.path.exists(files_aei[j]):
            os.remove(files_aei[j])

        with open('elem_acos') as f:
            with open(files_elem[j],'a') as f1:

                for l in f:
                    sb = re.sub(' +', ' ', l)
                    sp = sb.split(' ')

                    with open('number_names_final') as f2:
                        for l2 in f2:
                            sb2 = re.sub(' +', ' ', l2)
                            sp2 = sb2.split(' ')

                            if j == 0:

                                if sp[0] == '':
                                    if sp[1] == sp2[1]:
                                        if sp2[4] == '1\n':
                                            f1.write(l)
                                else:
                                    if sp[0] == sp2[0]:
                                        if sp2[3] == '1\n':
                                            f1.write(l)
                            elif j == 1:

                                if sp[0] == '':
                                    if sp[1] == sp2[1]:

                                        if sp2[4] == '2\n':
                                            f1.write(l)
                                else:
                                    if sp[0] == sp2[0]:
                                        if sp2[3] == '2\n':
                                            f1.write(l)
                            else:

                                if sp[0] == '':
                                    if sp[1] == sp2[1]:
                                        if sp2[4] == '3\n':
                                            f1.write(l)
                                else:
                                    if sp[0] == sp2[0]:
                                        if sp2[3] == '3\n':
                                            f1.write(l)

        with open('elem_acos') as f:
            with open(files_aei[j],'a') as f1:

                for l in f:
                    sb = re.sub(' +', ' ', l)
                    sp = sb.split(' ')

                    with open('number_names_final') as f2:
                        for l2 in f2:
                            sb2 = re.sub(' +', ' ', l2)
                            sp2 = sb2.split(' ')

                            if j == 0:

                                if sp[0] == '':
                                    if sp[1] == sp2[1]:
                                        if sp2[4] == '1\n':
                                            f1.write(l)
                                else:
                                    if sp[0] == sp2[0]:
                                        if sp2[3] == '1\n':
                                            f1.write(l)
                            elif j == 1:

                                if sp[0] == '':
                                    if sp[1] == sp2[1]:

                                        if sp2[4] == '2\n':
                                            f1.write(l)
                                else:
                                    if sp[0] == sp2[0]:
                                        if sp2[3] == '2\n':
                                            f1.write(l)
                            else:

                                if sp[0] == '':
                                    if sp[1] == sp2[1]:
                                        if sp2[4] == '3\n':
                                            f1.write(l)
                                else:
                                    if sp[0] == sp2[0]:
                                        if sp2[3] == '3\n':
                                            f1.write(l)


def elem_acos_final(elem_path,current_date):

    # Returns elements and TMOID for final ACOs

    if os.path.exists('elem_acos_final'):
        os.remove('elem_acos_final')
    if os.path.exists('elem_acos_final_aeiTMOID'):
        os.remove('elem_acos_final_aeiTMOID')

    os.system('ln -s ../../acos/' + current_date + '/number_names_final')

    orbits = ['jf','cen','aho']

    for j in orbits:

        if os.path.exists('elem_acos_' + j + '_final'):
            os.remove('elem_acos_' + j + '_final')

        shutil.copyfile(elem_path + '/elem_acos_' + j,'elem_acos_' + j + '_final')


    with open('elem_acos') as f:
        with open('elem_acos_final', 'a') as f1:

            for l in f:
                sb = re.sub(' +', ' ', l)
                sp = sb.split(' ')

                with open('number_names_final') as f2:
                    for l2 in f2:
                        sb2 = re.sub(' +', ' ', l2)
                        sp2 = sb2.split(' ')

                        if sp[0] == '':
                            if sp[1] == sp2[1]:
                                f1.write(l)
                        else:
                            if sp[0] == sp2[0]:
                                f1.write(l)

    with open('elem_acos_aeiTMOID') as f:
        with open('elem_acos_final_aeiTMOID', 'a') as f1:

            for l in f:
                sb = re.sub(' +', ' ', l)
                sp = sb.split(' ')

                with open('number_names_final') as f2:
                    for l2 in f2:
                        sb2 = re.sub(' +', ' ', l2)
                        sp2 = sb2.split(' ')

                        if sp[0] == '':
                            if sp[1] == sp2[1]:
                                f1.write(l)
                        else:
                            if sp[0] == sp2[0]:
                                f1.write(l)
