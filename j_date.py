import jdcal


def j_date(string):

    # Transforms the epoch from MPC format to julian date

    if string[0] == 'I':
        c0 = '18'
    elif string[0] == 'J':
        c0 = '19'
    elif string[0] == 'K':
        c0 = '20'

    c1 = string[1:3]

    if string[3] == 'A':
        c2 = '10'
    elif string[3] == 'B':
        c2 = '11'
    elif string[3] == 'C':
        c2 = '12'
    else:
        c2 = '0' + string[3]


    if string[4] == 'A':
        c3 = '10'
    elif string[4] == 'B':
        c3 = '11'
    elif string[4] == 'C':
        c3 = '12'
    elif string[4] == 'D':
        c3 = '13'
    elif string[4] == 'E':
        c3 = '14'
    elif string[4] == 'F':
        c3 = '15'
    elif string[4] == 'G':
        c3 = '16'
    elif string[4] == 'H':
        c3 = '17'
    elif string[4] == 'I':
        c3 = '18'
    elif string[4] == 'J':
        c3 = '19'
    elif string[4] == 'K':
        c3 = '20'
    elif string[4] == 'L':
        c3 = '21'
    elif string[4] == 'M':
        c3 = '22'
    elif string[4] == 'N':
        c3 = '23'
    elif string[4] == 'O':
        c3 = '24'
    elif string[4] == 'P':
        c3 = '25'
    elif string[4] == 'Q':
        c3 = '26'
    elif string[4] == 'R':
        c3 = '27'
    elif string[4] == 'S':
        c3 = '28'
    elif string[4] == 'T':
        c3 = '29'
    elif string[4] == 'U':
        c3 = '30'
    elif string[4] == 'V':
        c3 = '31'
    else:
        c3 = '0' + string[4]

    a = int(sum(jdcal.gcal2jd(c0+c1,c2,c3)))+0.5
    jdate = str(a)

    return jdate