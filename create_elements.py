import os.path
import re
from j_date import j_date


def convert_MPC(param):

    MPC_file = 'MPCORB.dat'

    # Remove file, file2, mpcorb.elem and mpcorb.elemprec if exists

    files = ['file','file2','elem_mpcorb','elem_mpcorb.prec']
    for i in range(len(files)):
        if os.path.isfile(files[i]):
            os.remove(files[i])

    # Remove the first 43 lines of MPC_file and save the result in file.elem

    with open('file', 'a') as f1:
        aux = 0
        for ln1 in open(MPC_file):
            sb = re.sub(' +', ' ', ln1)
            sp = sb.split(' ')
            aux = aux+1
            if (aux > 43) and len(sp)>1:
                f1.write(ln1)

    # Remove imprecise elements acording to MPC parameter (param)

    with open('file2', 'a') as f2:
        for ln2 in open('file'):
            sb = re.sub(' +', ' ', ln2)
            sp = sb.split(' ')
            if len(sp)>1:
                if (ln2[105]=='0') or (ln2[105]=='1') or (ln2[105]=='2') or (ln2[105]=='3') or (ln2[105]=='4') or \
                        (ln2[105]=='5') or (ln2[105]=='6') or (ln2[105]=='7') or (ln2[105]=='8') or (ln2[105]=='9'):
                    if int(ln2[105]) <= param:
                        f2.write(ln2)

    # Function that take the columns of interest and save them into a file

    def create_elem(file_in, file_out):

        with open (file_out,'a') as f3:
             with open(file_in, 'r') as f4:
                 count = 0
                 for ln3 in f4.readlines():
                     count = count + 1
                     sb = re.sub(' +', ' ', ln3)
                     sp = sb.split(' ')
                     parts = ln3.split(' ')
                     p00 = "%6d" % count
                     p0 = "%7s" % parts[0]
                     if ln3[9]==' ':
                         p1 = '\t'
                         p2 = ' '
                         p3 = "%9s" % j_date(sp[1])
                         p4 = "%10.5f" % float(sp[2])
                         p5 = "%10.5f" % float(sp[3])
                         p6 = "%10.5f" % float(sp[4])
                         p7 = "%10.5f" % float(sp[5])
                         p8 = "%10.7f" % float(sp[6])
                         if float(sp[8])<100:
                             p9 = "%10.7f" % float(sp[8])
                         elif float(sp[8])<1000:
                             p9 = "%10.6f" % float(sp[8])
                         else:
                             p9 = "%10.5f" % float(sp[8])
                     else:
                         p1 = "%5.2f" % float(sp[1])
                         p2 = "%5.2f" % float(sp[2])
                         p3 = "%9s" % j_date(sp[3])
                         p4 = "%10.5f" % float(sp[4])
                         p5 = "%10.5f" % float(sp[5])
                         p6 = "%10.5f" % float(sp[6])
                         p7 = "%10.5f" % float(sp[7])
                         p8 = "%10.7f" % float(sp[8])
                         if float(sp[10])<10:
                             p9 = "%10.7f" % float(sp[10])
                         elif float(sp[10])<100:
                             p9 = "%10.6f" % float(sp[10])
                         else:
                             p9 = "%10.5f" % float(sp[10])
                     if sp[-5][0]=='(':
                         p10 = sp[-5].replace('(','').replace(')','')
                     elif sp[-4][0]=='(':
                         p10 = sp[-4].replace('(','').replace(')','')
                     elif sp[-3][0]=='(':
                         p10 = sp[-3].replace('(','').replace(')', '')
                     else:
                         p10 = sp[-3] + sp[-2]
                     p10 = "%9s" % p10
                     f3.write(p00 + '  ' + p9 + '' + p8 + '' + p7 + '' + p6 + '' + p5 + '' + p4 + ' ' + p3 + ' ' + p1 +
                              ' ' + p2 + ' ' + p10 + ' ' + p0 + '\n')

    # Create files elem_mpcorb and elem_mpcorb.prec using create_elem

    create_elem('file','elem_mpcorb')
    create_elem('file2','elem_mpcorb.prec')
    os.remove('file')
    os.remove('file2')


def TMOID(compute_Tmoid,elem_planet,current_date,option):

    os.system('ln -s ' + elem_planet + ' ' + current_date)

    if option == 1:

        if os.path.isfile('aux0'):
            os.remove('aux0')
        if os.path.isfile('elem_ast_aeiTMOID'):
            os.remove('elem_ast_aeiTMOID')
        if os.path.isfile('elem_ast_aeiTMOID.prec'):
            os.remove('elem_ast_aeiTMOID.prec')

        with open('aux0', 'a') as f9:
            with open('elem_mpcorb.prec', 'r') as f10:
                for ln7 in f10.readlines():
                    ln7 = re.sub(' +', ' ', ln7)
                    parts = ln7.split(' ')
                    if parts[0]=='':
                        f9.write(parts[1] + ' ' + parts[2] + ' ' + parts[3] + ' ' + parts[4] + ' ' + parts[5] + ' ' +
                                 parts[6] + '\n')
                    else:
                        f9.write(parts[0] + ' ' + parts[1] + ' ' + parts[2] + ' ' + parts[3] + ' ' + parts[4] + ' ' +
                                 parts[5] + '\n')

        os.system(compute_Tmoid + '<aux0> elem_ast_aeiTMOID.prec')
        os.remove('aux0')

        with open('aux1', 'a') as f11:
            with open('elem_mpcorb', 'r') as f12:
                for ln8 in f12.readlines():
                    ln8 = re.sub(' +', ' ', ln8)
                    parts = ln8.split(' ')
                    if parts[0]=='':
                        f11.write(parts[1] + ' ' + parts[2] + ' ' + parts[3] + ' ' + parts[4] + ' ' + parts[5] + ' ' +
                                  parts[6] + '\n')
                    else:
                        f11.write(parts[0] + ' ' + parts[1] + ' ' + parts[2] + ' ' + parts[3] + ' ' + parts[4] + ' ' +
                                  parts[5] + '\n')

        os.system(compute_Tmoid + '<aux1> elem_ast_aeiTMOID')
        os.remove('aux1')


    elif option == 2:

        if os.path.isfile('aux2'):
            os.remove('aux2')
        if os.path.isfile('elem_com_aeiTMOID'):
            os.remove('elem_com_aeiTMOID')

        with open('aux2', 'a') as f12:
            with open('elem_jpl', 'r') as f13:
                for ln9 in f13.readlines():
                    ln9 = re.sub(' +', ' ', ln9)
                    parts = ln9.split(' ')
                    if parts[0] == '':
                        f12.write(
                            parts[1] + ' ' + parts[2] + ' ' + parts[3] + ' ' + parts[4] + ' ' + parts[5] + ' ' + parts[
                                6] + '\n')
                    else:
                        f12.write(
                            parts[0] + ' ' + parts[1] + ' ' + parts[2] + ' ' + parts[3] + ' ' + parts[4] + ' ' + parts[
                                5] + '\n')

        os.system(compute_Tmoid + '<aux2> elem_com_aeiTMOID')
        os.remove('aux2')
