import numpy as np
from scipy.special import factorial
import os


def criterion():

    def load_elem():

        # Load planet elements

        planet = np.loadtxt('elem_planet_orig')

        # Compute Hill's radii

        def hill(a,mu):
            # a - semimajor axis in au
            # mu - mass (in Solar masses)

            return a*np.power((mu/3/(1+mu)),(1/3))

        Hill = hill(planet[:,1],planet[:,0])

        # Load comet and asteroid elements and create new columns

        aux1 = np.loadtxt('elem_com_aeiTMOID')
        aux2 = np.loadtxt('elem_ast_aeiTMOID.prec')

        com = np.zeros((aux1.shape[0],19))
        ast = np.zeros((aux2.shape[0],19))

        com[:,:-7] = aux1
        ast[:,:-7] = aux2

        for i in range(4):
            com[:,i+13] = com[:,i*2+5]/Hill[i+4]

        for i in range(com.shape[0]):
            com[i,12] = np.min(com[i,[13,14,15,16]],axis=0)
            com[i,17] = com[i,1] * (1 - com[i,2])
            com[i,18] = com[i,1] * (1 + com[i,2])

        for i in range(4):
            ast[:,i+13] = ast[:,i*2+5]/Hill[i+4]

        for i in range(ast.shape[0]):
            ast[i,12] = np.min(ast[i,[13,14,15,16]],axis=0)
            ast[i,17] = ast[i,1] * (1 - ast[i,2])
            ast[i,18] = ast[i,1] * (1 + ast[i,2])

        # 0 - Number
        # 1 - a
        # 2 - e
        # 3 - i
        # 4 - T Jupiter
        # 5 - MOID Jupiter
        # 6 - T Saturn
        # 7 - MOID Saturn
        # 8 - T Uranus
        # 9 - MOID Uranus
        # 10 - T Neptune
        # 11 - MOID Neptune
        # 12 - min MOID
        # 13 - MOID/Hill Jupiter
        # 14 - MOID/Hill Saturn
        # 15 - MOID/Hill Uranus
        # 16 - MOID/Hill Neptune
        # 17 - q
        # 18 - Q

        return [planet,com,ast,Hill]


    [planet,com,ast,Hill] = load_elem()

    # Obtain planet parameters

    mu = planet[4 ,0]
    ajup = planet[4 ,1]
    ejup = planet[4 ,2]
    Qjup = np.multiply(ajup ,[ 1 +ejup])[0]
    qjup = np.multiply(ajup ,[ 1 -ejup])[0]


    # Set limits for T

    Tlow = 2
    Thigh = 3.05

    # JFCs have Tlow < T < Thigh & q < Qjup

    seljfc = np.where((com[:,4]<Thigh) & (com[:,4]>Tlow) & (com[:,17]<Qjup))[0]
    jfc = com[seljfc,:]

    # HTOs have T < Tlow

    selhto = np.where(com[:,4]<Tlow)[0]
    hto = com[selhto,:]

    # CAOs have T > Thigh &  q < Qjup

    selcao = np.where((com[:,4]>=Thigh) & (com[:,17]<Qjup))[0]
    cao = com[selcao,:]

    # Centaur comets have T > Tlow & q > Qjup

    selccen = np.where((com[:,4]>Tlow) & (com[:,17]>=Qjup))[0]
    ccen = com[selccen,:]


    # Define some functions for calculate the resonances


    def falpha(j,jp):

        return 1/np.power(np.abs(j/jp),(2/3))


    def laplacecoef(j,s,alpha):

        prods = 1
        for i in range(0,j):
            prods = prods*(s+i)

        sums = 1
        for i in range(1,101):
            coef = 1
            for ii in range(1,i+1):
                coef = coef*(s+ii-1)*(s+j+ii-1)/(j+ii)

            sums = sums + coef/factorial(i)*np.power(alpha,(i*2))

        return 2*prods/factorial(j)*np.power(alpha,j)*sums


    def der_laplacecoef(j,s,alpha,n):

        if n == 0:
            Db = laplacecoef(j,s,alpha)
        elif n == 1:
            Db = s*(laplacecoef(j-1,s+1,alpha) - 2*alpha*laplacecoef(j,s+1,alpha) + laplacecoef(j+1,s+1,alpha))
        else:
            Db = s*(der_laplacecoef(j-1,s+1,alpha,n-1) - 2*alpha*der_laplacecoef(j,s+1,alpha,n-1) +
                    der_laplacecoef(j+1,s+1,alpha,n-1) - 2*(n-1)*der_laplacecoef(j,s+1,alpha,n-2))

        return Db


    def alphafunction(j,jp):

        alpha = falpha(j,jp)
        j2 = j*j
        j3 = j*j2
        j4 = j*j3

        if j + jp == 1:
            afd = alpha/2*(-2*j*laplacecoef(j,1/2,alpha) - alpha*der_laplacecoef(j,1/2,alpha,1))
        elif j + jp == 2:
            afd = alpha/8*((-5*j+4*j2)*laplacecoef(j,1/2,alpha) + (-2+4*j)*alpha*der_laplacecoef(j,1/2,alpha,1) +
                           alpha**2*der_laplacecoef(j,1/2,alpha,2))
        elif j + jp == 3:
            afd = alpha/48*((-26*j+30*j2-8*j3)*laplacecoef(j,1/2,alpha) +
                            (-9+27*j-12*j2)*alpha*der_laplacecoef(j,1/2,alpha,1) +
                            (6-6*j)*alpha**2*der_laplacecoef(j,1/2,alpha,2) - alpha**3*der_laplacecoef(j,1/2,alpha,3))
        elif j + jp == 4:
            afd = alpha/384*((-206*j+283*j2-120*j3+16*j4)*laplacecoef(j,1/2,alpha) +
                             (-64+236*j-168*j2+32*j3)*alpha*der_laplacecoef(j,1/2,alpha,1) +
                             (48-78*j+24*j2)*alpha**2*der_laplacecoef(j,1/2,alpha,2) +
                             (-12+8*j)*alpha**3*der_laplacecoef(j,1/2,alpha,3)+alpha**4*der_laplacecoef(j,1/2,alpha,4))

        return afd


    # Calculate resonances (See Supplementary Material of Tancredi, 2014)

    jres = np.array([[4, -1], [3, -1], [5, -2], [7, -3], [2, -1]])

    alpha = np.zeros(jres.shape[0])
    afd = np.zeros(jres.shape[0])

    for i in range(jres.shape[0]):
        alpha[i] = falpha(jres[i,0],jres[i,1])
        afd[i] = alphafunction(jres[i,0],jres[i,1])

    crn = abs(mu*afd)
    a = ajup*alpha
    elow = 0.01
    ebig = 0.3
    deltahilda = 0.1
    ahilda = ajup*falpha(3,-2)
    deltathule = 0.1
    athule = ajup*falpha(4,-3)
    deltatrojanrel = 0.04

    aa = ast[:,1]
    ea = ast[:,2]
    MOIDa = ast[:,12]

    iclose = []
    rclose = []

    deltaa = np.zeros((len(ea),2))
    deltaalow = np.zeros(2)
    deltaabig = np.zeros(2)
    selel = np.where(ea < elow)[0]
    seleb = np.where(ea > ebig)[0]

    for i in range(len(afd)):
        if jres[i,0] + jres[i,1] == 1:
            for k in range(len(ea)):
                deltaa[k,0] = -a[i]*(-2/9*crn[i]/jres[i,1]/ea[k] -
                                     np.sqrt(16/3*crn[i]*ea[k])*np.sqrt(1+crn[i]/27/jres[i,1]/jres[i,1]/ea[k]**3))
                deltaa[k,1] = a[i]*(-2/9*crn[i]/jres[i,1]/ea[k] +
                                     np.sqrt(16/3*crn[i]*ea[k])*np.sqrt(1+crn[i]/27/jres[i,1]/jres[i,1]/ea[k]**3))

            deltaalow[0] = -a[i]*(-2/9*crn[i]/jres[i,1]/elow -
                                 np.sqrt(16/3*crn[i]*elow)*np.sqrt(1+crn[i]/27/jres[i,1]/jres[i,1]/elow**3))
            deltaalow[1] = a[i]*(-2/9*crn[i]/jres[i,1]/elow +
                                 np.sqrt(16/3*crn[i]*elow)*np.sqrt(1+crn[i]/27/jres[i,1]/jres[i,1]/elow**3))

            deltaa[selel,0] = np.ones(selel.shape)*deltaalow[0]
            deltaa[selel,1] = np.ones((selel.shape))*deltaalow[1];
            deltaabig[0] = -a[i]*(-2/9*crn[i]/jres[i,1]/ebig - np.sqrt(16/3*crn[i]*ebig)*np.sqrt(1+crn[i]/27/jres[i,1]/jres[i,1]/ebig**3))
            deltaabig[1] = a[i]*(-2/9*crn[i]/jres[i,1]/ebig + np.sqrt(16/3*crn[i]*ebig)*np.sqrt(1+crn[i]/27/jres[i,1]/jres[i,1]/ebig**3))
            deltaa[seleb,0] = np.ones((seleb.shape))*deltaabig[0]
            deltaa[seleb,1] = np.ones((seleb.shape))*deltaabig[1]

        elif jres[i,0] + jres[i,1] >= 2:
            for k in range(len(ea)):
                deltaa[k,0] = a[i]*np.sqrt(16/3*crn[i]*ea[k]**(jres[i,0] + jres[i,1]))
                deltaa[k,1] = deltaa[k,0]
            deltaabig[0] = a[i]*np.sqrt(16/3*crn[i]*ebig**(jres[i,0] + jres[i,1]))
            deltaabig[1] = deltaabig[0]

            deltaa[seleb,0] = deltaabig[0]*np.ones((seleb.shape))
            deltaa[seleb,1] = deltaa[seleb,0]

        nclose = np.where((aa - (a[i]-deltaa[:,0]) > 0) & (aa - (a[i] + deltaa[:,1]) < 0) & (aa*(1 + ea) < Qjup + Hill[4])
                      & (~((aa*(1 + ea) > (qjup - Hill[4]/4)) & (MOIDa < 1.5))))[0]

        for h in range(len(iclose),len(iclose) + len(nclose)):
            rclose.append(i)

        for l in range(len(nclose)):
            iclose.append(nclose[l])


    # Remove Hildas
    i = i + 1
    nclose = np.where((np.abs(aa - ahilda) < deltahilda) & (aa*(1 + ea) < Qjup + Hill[4]/4))[0]
    for h in range(len(iclose),len(iclose) + len(nclose)+1):  # agregué +1 para ajustar
        rclose.append(i)
    for l in range(len(nclose)):
        iclose.append(nclose[l])

    # Remove Thule
    i = i + 1
    nclose = np.where((np.abs(aa - athule) < deltathule) & (aa*(1 + ea) < qjup - Hill[4]/2))[0]
    for h in range(len(iclose),len(iclose)+len(nclose)):
        rclose.append(i)
    for l in range(len(nclose)):
        iclose.append(nclose[l])

    # Remove Trojans
    i = i + 1
    nclose = np.where((np.abs(aa - ajup) < ajup*deltatrojanrel) & (ea < 0.35))[0]
    for h in range(len(iclose),len(iclose)+len(nclose)):
        rclose.append(i)
    for l in range(len(nclose)):
        iclose.append(nclose[l])

    icluni = np.unique(iclose)
    iclose = icluni
    astres = ast[iclose,:]
    inner = np.where(((ast[:,12] > 1) & (ast[:,1] < a[2])) | ((ast[:,12] > 2) & (ast[:,1] < a[3])))[0]
    astsinres = np.delete(ast,(iclose),axis=0)
    aux = np.append(icluni,inner)
    icluni = np.unique(aux)
    ast2 = np.delete(ast,(icluni),axis=0)


    # ACOs have T < Thigh & are below line MOID=0.75-(T-3)*0.75/.3=8.25-2.5*T
    # ACOs have Tlow < T < Thigh & ((MOID < MOIDlim & Q>qjup) | (MOID<2 & Q>qjup-1.5Hilljup)) & q<Qjup
    # ACOs have Tlow < T < Thigh & (MOID < MOIDlim & Q>qjup-Hilljup) & q<Qjup+Hilljup

    MOIDlim = 4
    MOIDlim2 = 2.5
    HillLim = 1.5

    iajf = np.where((ast2[:,4] > Tlow) & (ast2[:,4] < Thigh) &
                    (((ast2[:,12] < MOIDlim) & (ast2[:,18] > qjup)) |
                     ((ast2[:,12] < MOIDlim2) & (ast2[:,18] > qjup - HillLim*Hill[4]))) & (ast2[:,17] < Qjup))[0]


    ajf = ast2[iajf,:]
    ast2 = np.delete(ast2,(iajf),axis=0)


    # Halley type: T < Tlow
    iaho = np.where(ast2[:,4] < Tlow)[0]
    aho = ast2[iaho,:]
    ast2 = np.delete(ast2,(iaho),axis=0)


    # Centaurs orbits:  T > Tlow & q > QJup & q < aUra
    icen = np.where((ast2[:,4] > Tlow) & (ast2[:,17] > Qjup) & (ast2[:,17] < planet[6,1]))[0]
    cen = ast2[icen,:]
    ast2 = np.delete(ast2,(icen),axis=0)
    iT3 = np.where(ast2[:,4] < Thigh)[0]


    # JFC that not fulfill the criteria for ACO JF
    inojfc = np.where(~((jfc[:,4] > Tlow) & (jfc[:,4] < Thigh) & (((jfc[:,12] < MOIDlim) & (jfc[:,18] > qjup)) |
                ((jfc[:,12] < MOIDlim2) & (jfc[:,18] > qjup - HillLim*Hill[4]))) & (jfc[:,17] < Qjup)))[0]
    nojfc = jfc[np.where(~((jfc[:,4] > Tlow) & (jfc[:,5] < Thigh) & (((jfc[:,12] < MOIDlim) & (jfc[:,18] > qjup)) |
                ((jfc[:,12] < MOIDlim2) & (jfc[:,18] > qjup - HillLim*Hill[4]))) & (jfc[:,17] < Qjup)))[0],0]


    # Save the results in txt files

    if os.path.isfile("acos.txt"):
        os.remove("acos.txt")

    if os.path.isfile("comet.txt"):
        os.remove("comet.txt")

    with open('acos.txt','a') as f1:
        for i in range(len(ajf)):
            f1.write("%3.3d 1\n" % ajf[i,0])
        for j in range(len(cen)):
            f1.write("%3.3d 2\n" % cen[j,0])
        for k in range(len(aho)):
            f1.write("%3.3d 3\n" % aho[k,0])

    with open('comet.txt','a') as f2:
        for i in range(len(jfc)):
            f2.write("%3.3d 1\n" % jfc[i,0])
        for j in range(len(ccen)):
            f2.write("%3.3d 2\n" % ccen[j,0])
        for k in range(len(hto)):
            f2.write("%3.3d 3\n" % hto[k,0])
        for h in range(len(cao)):
            f2.write("%3.3d 4\n" % cao[h,0])
