import re
import os
import jdcal
import requests
from astroquery.jplhorizons import Horizons
from configuration import config


def web_table():

    current_date, results, programs, option = config()

    YYYY = current_date[3:7]
    MM = current_date[0:2]
    DD = '01'
    date = int(sum(jdcal.gcal2jd(YYYY, MM, DD))) + 0.5

    if os.path.exists('acos_web'):
        os.remove('acos_web')

    acos = []

    with open('acos_web','a') as f1:
        with open('elem_acos_final','r') as f2:
            for l2 in f2.readlines():
                l2 = re.sub(' +', ' ', l2)
                p2 = l2.split(' ')

                if p2[0] == '':
                    name = p2[11]

                else:
                    if len(p2[11]) < 7:
                        name = p2[10]
                    else:
                        name = p2[10][0:4] + ' ' + p2[10][4:]

                try:
                    obj = Horizons(id=name, location='500', epochs=date)
                    eph = obj.ephemerides(quantities='1,9')
                except:
                    print("Object " + name + "not found in JPL Horizons")
                    continue

                RA = eph['RA'][0]
                DEC = eph['DEC'][0]
                try:
                    V = eph['V'][0]
                except:
                    V = eph['Tmag'][0]

                with open('elem_acos_final_aeiTMOID', 'r') as f3:
                    for l3 in f3.readlines():
                        l3 = re.sub(' +', ' ', l3)
                        p3 = l3.split(' ')

                        with open('number_names_final','r') as f4:
                            for l4 in f4.readlines():
                                l4 = re.sub(' +', ' ', l4)
                                p4 = l4.split(' ')

                                if p2[0] == '':

                                    if p2[1] == p3[1]:
                                        if p2[1] == p4[1]:

                                            f1.write("%9s" % p2[11] + ' ' + "%7s" % p2[12] + ' ' + "%7.3f" % float(p2[2]) +
                                                     ' ' + "%5.3f" % float(p2[3]) + ' ' + "%7.3f" % float(p2[4]) + ' ' +
                                                     "%5.2f" % float(p2[9]) + ' ' + "%5.2f" % float(p2[10]) + ' ' + "%6.3f"
                                                     % float(p3[5]) + ' ' + "%6.3f" % float(p3[6]) + ' ' + "%1d" %
                                                     float(p4[4]) + ' ' + "%6.2f" % float(RA) + ' ' + "%6.2f" % float(DEC)
                                                     + ' ' + "%5.2f" % float(V) + '\n')

                                            ID = p2[1]
                                            name = "%s" % p2[11]
                                            packed = "%s" % p2[12]
                                            a = "%.3f" % float(p2[2])
                                            e = "%.3f" % float(p2[3])
                                            i = "%.3f" % float(p2[4])
                                            H = "%.2f" % float(p2[9])
                                            G = "%.2f" % float(p2[10])
                                            T = "%.3f" % float(p3[5])
                                            MOID = "%.3f" % float(p3[6])
                                            acotype = "%d" % float(p4[4])
                                            ra = "%.2f" % float(RA)
                                            dec = "%.2f" % float(DEC)
                                            v = "%.2f" % float(V)

                                            if acotype == '1':
                                                acotype = 'Jupiter Family'
                                            else:
                                                if acotype == '2':
                                                    acotype = 'Centaur'
                                                else:
                                                    if acotype == '3':
                                                        acotype = 'Halley Type'

                                            aco = {"a": a, "e": e, "i": i, "internalID": ID, "name": name, "packed": packed,
                                                   "H": H, "G": G, "T": T, "MOID": MOID, "acotype": acotype, "ra": ra,
                                                   "dec": dec, "v": v}
                                            acos.append(aco)



                                else:
                                    if p2[0] == p3[0]:
                                        if p2[0] == p4[0]:

                                            f1.write("%9s" % p2[10] + ' ' + "%7s" % p2[11] + ' ' + "%7.3f" % float(p2[1]) +
                                                     ' ' + "%5.3f" % float(p2[2]) + ' ' + "%7.3f" % float(p2[3]) + ' ' +
                                                     "%5.2f" % float(p2[8]) + ' ' + "%5.2f" % float(p2[9]) + ' ' + "%6.3f"
                                                     % float(p3[4]) + ' ' + "%6.3f" % float(p3[5]) + ' ' + "%1d" %
                                                     float(p4[3]) + ' ' + "%6.2f" % float(RA) + ' ' + "%6.2f" % float(DEC)
                                                     + ' ' + "%5.2f" % float(V) + '\n')

                                            ID = p2[0]
                                            name = "%s" % p2[10]
                                            packed = "%s" % p2[11]
                                            a = "%.3f" % float(p2[1])
                                            e = "%.3f" % float(p2[2])
                                            i = "%.3f" % float(p2[3])
                                            H = "%.2f" % float(p2[8])
                                            G = "%.2f" % float(p2[9])
                                            T = "%.3f" % float(p3[4])
                                            MOID = "%.3f" % float(p3[5])
                                            acotype = "%d" % float(p4[3])
                                            ra = "%.2f" % float(RA)
                                            dec = "%.2f" % float(DEC)
                                            v = "%.2f" % float(V)

                                            if acotype == '1':
                                                acotype = 'Jupiter Family'
                                            else:
                                                if acotype == '2':
                                                    acotype = 'Centaur'
                                                else:
                                                    if acotype == '3':
                                                        acotype = 'Halley Type'

                                            aco = {"a": a, "e": e, "i": i, "internalID": ID, "name": name, "packed": packed,
                                                   "H": H, "G": G, "T": T, "MOID": MOID, "acotype": acotype, "ra": ra,
                                                   "dec": dec, "v": v}
                                            acos.append(aco)



    json={"acos": acos}
    r = requests.post('https://acos-web.duckdns.org/api/create_acos', json=json)
